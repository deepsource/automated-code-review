﻿// <copyright file="SendEmailRequest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KocSistem.CommunityEdition.WebAPI.Model.EmailTemplate
{
    public class SendEmailRequest
    {
        public string To { get; set; }

        public string Subject { get; set; }

        public string Content { get; set; }
    }
}
