﻿// <copyright file="ClaimResponse.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KocSistem.CommunityEdition.WebAPI.Model.ClaimHelper
{
    public class ClaimResponse
    {
        public ClaimResponse()
        {
        }

        public string Name { get; set; }

        public string Value { get; set; }
    }
}