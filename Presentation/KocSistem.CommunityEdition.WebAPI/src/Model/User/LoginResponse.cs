// <copyright file="LoginResponse.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.WebAPI.Model.ClaimHelper;

namespace KocSistem.CommunityEdition.WebAPI.Model.User
{
    public class LoginResponse
    {
        public string Token { get; set; }

        public string RefreshToken { get; set; }

        public IList<ClaimResponse> Claims { get; set; }
    }
}