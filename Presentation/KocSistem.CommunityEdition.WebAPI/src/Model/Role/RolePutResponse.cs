﻿// <copyright file="RolePutResponse.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KocSistem.CommunityEdition.WebAPI.Model.Role
{
    public class RolePutResponse
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}