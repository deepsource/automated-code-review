﻿// <copyright file="CookieHelper.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KocSistem.CommunityEdition.WebAPI.Helper
{
    public static class CookieHelper
    {
        public const string TimeZone = "TimeZone";
    }
}
