// <copyright file="IClaimHelper.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Mvc.Models.Account;

namespace KocSistem.CommunityEdition.Mvc.Helpers
{
    public interface IClaimHelper
    {
        Task BuildClaimsAndSignIn(LoginResponseViewModel loginResponse);
    }
}
