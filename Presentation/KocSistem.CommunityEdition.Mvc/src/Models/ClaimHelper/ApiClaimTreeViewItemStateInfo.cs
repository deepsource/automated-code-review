﻿// <copyright file="ApiClaimTreeViewItemStateInfo.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KocSistem.CommunityEdition.Mvc.Models.ClaimHelper
{
    public class ApiClaimTreeViewItemStateInfo
    {
        public bool Disabled { get; set; }

        public bool Opened { get; set; }

        public bool Selected { get; set; }
    }
}
