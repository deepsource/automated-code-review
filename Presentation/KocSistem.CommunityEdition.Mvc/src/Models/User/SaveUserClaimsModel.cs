﻿// <copyright file="SaveUserClaimsModel.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using System.ComponentModel.DataAnnotations;

namespace KocSistem.CommunityEdition.Mvc.Models.User
{
    public class SaveUserClaimsModel
    {
        [Required]
        public string Name { get; set; }

        public List<string> SelectedUserClaimList { get; set; }
    }
}