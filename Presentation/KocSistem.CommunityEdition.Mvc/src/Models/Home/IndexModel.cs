﻿// <copyright file="IndexModel.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Mvc.Models.Profile;

namespace KocSistem.CommunityEdition.Mvc.Models.Home
{
    public class IndexModel
    {
        public ProfileModel Profile { get; set; }
    }
}
