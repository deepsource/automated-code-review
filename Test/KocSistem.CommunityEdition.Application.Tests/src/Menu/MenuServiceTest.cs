﻿// <copyright file="MenuServiceTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.Menu.Contracts;
using KocSistem.CommunityEdition.Application.Menu;
using KocSistem.CommunityEdition.Common.Helpers;
using KocSistem.OneFrame.Data;
using KocSistem.OneFrame.Data.Relational;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Localization;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Application.Tests.Menu
{
    public class MenuServiceTest
    {
        private readonly Mock<IDataManager> _dataManagerMoq;
        private readonly Mock<IClaimManager> _claimManagerMoq;
        private readonly Mock<IRepository<Domain.Menu>> _menuRepositoryMoq;
        private readonly Mock<MenuService> _menuServiceMoq;
        private readonly Mock<IMapper> _mapperMoq;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;

        public MenuServiceTest()
        {
            _dataManagerMoq = new Mock<IDataManager>();
            _claimManagerMoq = new Mock<IClaimManager>();
            _menuRepositoryMoq = new Mock<IRepository<Domain.Menu>>();
            _menuServiceMoq = new Mock<MenuService>();
            _mapperMoq = new Mock<IMapper>();
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();
        }

        [Fact]
        [Trait("Category", "MenuService")]
        public async Task GetUserMenuList_ReturnsSuccessfulResponse()
        {
            var menusServiceResponse = new ServiceResponse<IEnumerable<MenuDto>>(null, true);
            _ = _menuServiceMoq.Setup(mock => mock.GetListAsync(
                  It.IsAny<Expression<Func<Domain.Menu, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.Menu>, IOrderedQueryable<Domain.Menu>>>(),
                  It.IsAny<Func<IQueryable<Domain.Menu>, IIncludableQueryable<Domain.Menu, object>>>())).ReturnsAsync(menusServiceResponse);

            _ = _mapperMoq.Setup(r => r.Map<IList<MenuDto>, IList<UserMenuDto>>(It.IsAny<MenuDto[]>())).Returns(new List<UserMenuDto>() { new UserMenuDto() {ParentId=1 } });
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<IList<UserMenuDto>>())).Returns(new ServiceResponse<IList<UserMenuDto>>(new List<UserMenuDto>(), true));
            var service = CreateMenuService();

            var response = await service.GetUserMenuListAsync().ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Empty(response.Result);
        }

        [Fact]
        [Trait("Category", "MenuService")]
        public async Task GetMenuTree_MenuNotFound_ReturnsUnsuccessfulResponse()
        {
            var result = new List<Domain.Menu>();
            var menuDto = new List<MenuDto>();

            _ = _menuRepositoryMoq.Setup(r => r.GetListAsync(
                  It.IsAny<Expression<Func<Domain.Menu, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.Menu>, IOrderedQueryable<Domain.Menu>>>(),
                  It.IsAny<Func<IQueryable<Domain.Menu>, IIncludableQueryable<Domain.Menu, object>>>(),
                  It.IsAny<bool>(),
                  It.IsAny<CancellationToken>())).ReturnsAsync(result);

            _ = _mapperMoq.Setup(r => r.Map<List<Domain.Menu>, List<MenuDto>>(It.IsAny<List<Domain.Menu>>())).Returns(menuDto);

            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<List<MenuTreeViewItemDto>>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<List<MenuTreeViewItemDto>>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateMenuService();

            var response = await service.GetMenuTreeAsync().ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "MenuService")]
        public async Task GetMenuTree_ReturnsSuccessfulResponse()
        {
            var result = new List<Domain.Menu>();
            var menuDto = new List<MenuDto>
            {
                new MenuDto
                {
                    Id = 1,
                    ClaimType = null,
                    ClaimValue = null,
                    Icon = "menu-icon",
                    Name = "Menu name",
                    OrderId = 0,
                    ParentId = null,
                    Url = null,
                },
                new MenuDto
                {
                    Id = 2,
                    ClaimType = "KsPermission",
                    ClaimValue = "Menu_Claim",
                    Icon = "menu-icon-2",
                    Name = "Menu name 2",
                    OrderId = 1,
                    ParentId = 1,
                    Url = "menu-url",
                },
            };

            _ = _menuRepositoryMoq.Setup(r => r.GetListAsync(
                  It.IsAny<Expression<Func<Domain.Menu, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.Menu>, IOrderedQueryable<Domain.Menu>>>(),
                  It.IsAny<Func<IQueryable<Domain.Menu>, IIncludableQueryable<Domain.Menu, object>>>(),
                  It.IsAny<bool>(),
                  It.IsAny<CancellationToken>())).ReturnsAsync(result);

            _ = _mapperMoq.Setup(r => r.Map<List<Domain.Menu>, List<MenuDto>>(It.IsAny<List<Domain.Menu>>())).Returns(menuDto);

            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<List<MenuTreeViewItemDto>>())).Returns(new ServiceResponse<List<MenuTreeViewItemDto>>(new List<MenuTreeViewItemDto>(), true));
            var service = CreateMenuService();

            var response = await service.GetMenuTreeAsync().ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Empty(response.Result);
        }

        [Fact]
        [Trait("Category", "MenuService")]
        public async Task SaveMenuOrder_MenuListNull_ReturnsUnsuccessfulResponse()
        {
            SaveMenuOrderDto saveMenuOrderDto = new SaveMenuOrderDto
            {
                MenuList = null,
            };
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateMenuService();

            var response = await service.SaveMenuOrderAsync(saveMenuOrderDto);
            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "MenuService")]
        public async Task SaveMenuOrder_MenuListEmpty_ReturnsUnsuccessfulResponse()
        {
            SaveMenuOrderDto saveMenuOrderDto = new SaveMenuOrderDto
            {
                MenuList = new List<SaveMenuOrderItemDto>(),
            };
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateMenuService();

            var response = await service.SaveMenuOrderAsync(saveMenuOrderDto);
            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "MenuService")]
        public async Task SaveMenuOrder_ReturnsSuccessfulResponse()
        {
            var menuToUpdate = new List<Domain.Menu>()
            {
                new Domain.Menu()
                {
                    Id = 1,
                },
                new Domain.Menu()
                {
                    Id = 2,
                },
            };
            SaveMenuOrderDto saveMenuOrderDto = new SaveMenuOrderDto
            {
                MenuList = new List<SaveMenuOrderItemDto>()
                {
                    new SaveMenuOrderItemDto()
                    {
                        Id = 1,
                        ParentId = null,
                        OrderId = 0,
                    },
                    new SaveMenuOrderItemDto()
                    {
                        Id = 2,
                        ParentId = 1,
                        OrderId = 1,
                    },
                },
            };

            _ = _menuRepositoryMoq.Setup(r => r.GetListAsync(
                  It.IsAny<Expression<Func<Domain.Menu, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.Menu>, IOrderedQueryable<Domain.Menu>>>(),
                  It.IsAny<Func<IQueryable<Domain.Menu>, IIncludableQueryable<Domain.Menu, object>>>(),
                  It.IsAny<bool>(),
                  It.IsAny<CancellationToken>())).ReturnsAsync(menuToUpdate);

            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse());
            var service = CreateMenuService();

            var response = await service.SaveMenuOrderAsync(saveMenuOrderDto);
            Assert.True(response.IsSuccessful);
        }

        private MenuService CreateMenuService(List<Claim> claims = null)
        {
            var localizationMoq = new Mock<IKsStringLocalizer<MenuService>>();
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>(), It.IsAny<object[]>()]).Returns(new LocalizedString("sometext", "sometext"));

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IMapper))).Returns(_mapperMoq.Object);
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper))).Returns(_serviceResponseHelperMoq.Object);

            var service = new MenuService(
                    localize: localizationMoq.Object,
                    claimManager: _claimManagerMoq.Object,
                    menuRepository: _menuRepositoryMoq.Object,
                    mapper: _mapperMoq.Object,
                    dataManager: _dataManagerMoq.Object,
                    serviceResponseHelper: _serviceResponseHelperMoq.Object);

            if (claims != null)
            {
                var identity = new ClaimsIdentity(claims: claims, authenticationType: "Test");

                var mockPrincipal = new Mock<IPrincipal>();
                _ = mockPrincipal.Setup(expression: x => x.Identity).Returns(value: identity);
                _ = mockPrincipal.Setup(expression: x => x.IsInRole(It.IsAny<string>())).Returns(value: true);
            }

            return service;
        }
    }
}