﻿// <copyright file="UserConfirmationHistoryTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.UserConfirmationHistory.Contract;
using KocSistem.CommunityEdition.Application.UserConfirmationHistory;
using KocSistem.CommunityEdition.Common.Enums;
using KocSistem.OneFrame.Data;
using KocSistem.OneFrame.Data.Relational;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Localization;
using Moq;
using System;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Application.Tests.src.UserConfirmationHistory
{
    public class UserConfirmationHistoryServiceTest
    {
        private readonly Mock<IMapper> _mapperMoq;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;
        private readonly Mock<IRepository<Domain.UserConfirmationHistory>> _repositoryMoq;
        private readonly Mock<IDataManager> _dataManagerMoq;

        public UserConfirmationHistoryServiceTest()
        {
            _mapperMoq = new Mock<IMapper>();
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();
            _repositoryMoq = new Mock<IRepository<Domain.UserConfirmationHistory>>();
            _dataManagerMoq = new Mock<IDataManager>();
        }

        [Fact]
        [Trait("Category", "UserConfirmationHistoryService")]
        public async Task CreateUserConfirmationHistory_Success_ReturnSuccessfullResponse()
        {
            var confirmationHistoryDto = CreateDummyConfirmationHistoryDto();
            var confirmationHistory = CreateDummyConfirmationHistory();

            _ = _mapperMoq
                .Setup(r => r.Map<Domain.UserConfirmationHistory>(It.IsAny<UserConfirmationHistoryDto>()))
                .Returns(confirmationHistory);

            _ = _repositoryMoq
                .Setup(r => r.AddAsync(It.IsAny<Domain.UserConfirmationHistory>(), It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask);

            _ = _serviceResponseHelperMoq
                .Setup(r => r.SetSuccess(It.IsAny<UserConfirmationHistoryDto>()))
                .Returns(new ServiceResponse<UserConfirmationHistoryDto>(confirmationHistoryDto, true));

            var service = CreateUserConfirmationHistoryService();

            var response = await service.CreateCodeAsync(confirmationHistoryDto).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(confirmationHistoryDto.Code, response.Result.Code);
            Assert.Equal(confirmationHistoryDto.Id, response.Result.Id);
            Assert.Equal(confirmationHistoryDto.PhoneNumber, response.Result.PhoneNumber);
            Assert.Equal(confirmationHistoryDto.UserId, response.Result.UserId);
        }

        [Fact]
        [Trait("Category", "UserConfirmationHistoryService")]
        public async Task CheckCodeAsSentAsync_Success_ReturnSuccessfulResponse()
        {
            var confirmationHistoryGuid = Guid.NewGuid();

            var confirmationHistory = CreateDummyConfirmationHistory();
            confirmationHistory.Id = confirmationHistoryGuid;

            _ = _repositoryMoq.Setup(s => s.GetFirstOrDefaultAsync(
                    It.IsAny<Expression<Func<Domain.UserConfirmationHistory, bool>>>(),
                    It.IsAny<Func<IQueryable<Domain.UserConfirmationHistory>, IOrderedQueryable<Domain.UserConfirmationHistory>>>(),
                    It.IsAny<Func<IQueryable<Domain.UserConfirmationHistory>, IIncludableQueryable<Domain.UserConfirmationHistory, object>>>(),
                    It.IsAny<bool>())
                ).ReturnsAsync(confirmationHistory);

            _ = _repositoryMoq
                .Setup(s => s.UpdateAsync(It.IsAny<Domain.UserConfirmationHistory>(), It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask);

            _ = _serviceResponseHelperMoq
                .Setup(r => r.SetSuccess())
                .Returns(new ServiceResponse());

            var service = CreateUserConfirmationHistoryService();

            var response = await service.CheckCodeAsSentAsync(confirmationHistory.Id).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserConfirmationHistoryService")]
        public async Task CheckCodeAsSentAsync_CodeNotFound_ReturnUnsuccessfulResponse()
        {
            Domain.UserConfirmationHistory confirmationHistory = null;

            _ = _repositoryMoq.Setup(s => s.GetFirstOrDefaultAsync(
                    It.IsAny<Expression<Func<Domain.UserConfirmationHistory, bool>>>(),
                    It.IsAny<Func<IQueryable<Domain.UserConfirmationHistory>, IOrderedQueryable<Domain.UserConfirmationHistory>>>(),
                    It.IsAny<Func<IQueryable<Domain.UserConfirmationHistory>, IIncludableQueryable<Domain.UserConfirmationHistory, object>>>(),
                    It.IsAny<bool>())
                ).ReturnsAsync(confirmationHistory);

            _ = _serviceResponseHelperMoq
                .Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>()))
                .Returns(new ServiceResponse(new ErrorInfo { Code = StatusCodes.Status400BadRequest }, false));

            var service = CreateUserConfirmationHistoryService();

            var response = await service.CheckCodeAsSentAsync(Guid.NewGuid()).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(400, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserConfirmationHistoryService")]
        public async Task ConfirmCodeAsync_Success_ReturnSuccessfulResponse()
        {
            var confirmationHistory = CreateDummyConfirmationHistory();
            var confirmationHistoryDto = CreateDummyConfirmationHistoryDto();

            _ = _mapperMoq
                .Setup(r => r.Map<Domain.UserConfirmationHistory>(It.IsAny<UserConfirmationHistoryDto>()))
                .Returns(confirmationHistory);

            _ = _repositoryMoq.Setup(s => s.GetFirstOrDefaultAsync(
                    It.IsAny<Expression<Func<Domain.UserConfirmationHistory, bool>>>(),
                    It.IsAny<Func<IQueryable<Domain.UserConfirmationHistory>, IOrderedQueryable<Domain.UserConfirmationHistory>>>(),
                    It.IsAny<Func<IQueryable<Domain.UserConfirmationHistory>, IIncludableQueryable<Domain.UserConfirmationHistory, object>>>(),
                    It.IsAny<bool>())
                ).ReturnsAsync(confirmationHistory);

            _ = _repositoryMoq
                .Setup(s => s.UpdateAsync(It.IsAny<Domain.UserConfirmationHistory>(), It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask);

            _ = _serviceResponseHelperMoq
                .Setup(r => r.SetSuccess())
                .Returns(new ServiceResponse());

            var service = CreateUserConfirmationHistoryService();

            var response = await service.ConfirmCodeAsync(confirmationHistoryDto).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserConfirmationHistoryService")]
        public async Task GetActiveCodeAsync_Success_ReturnSuccessfulResponse()
        {
            var confirmationCodeHistory = CreateDummyConfirmationHistory();
            var confirmationCodeHistoryDto = CreateDummyConfirmationHistoryDto();

            _ = _repositoryMoq.Setup(s => s.GetFirstOrDefaultAsync(
                    It.IsAny<Expression<Func<Domain.UserConfirmationHistory, bool>>>(),
                    It.IsAny<Func<IQueryable<Domain.UserConfirmationHistory>, IOrderedQueryable<Domain.UserConfirmationHistory>>>(),
                    It.IsAny<Func<IQueryable<Domain.UserConfirmationHistory>, IIncludableQueryable<Domain.UserConfirmationHistory, object>>>(),
                    It.IsAny<bool>())
                ).ReturnsAsync(confirmationCodeHistory);

            _ = _mapperMoq
                .Setup(r => r.Map<UserConfirmationHistoryDto>(It.IsAny<Domain.UserConfirmationHistory>()))
                .Returns(confirmationCodeHistoryDto);

            _ = _serviceResponseHelperMoq
                .Setup(r => r.SetSuccess(It.IsAny<UserConfirmationHistoryDto>()))
                .Returns(new ServiceResponse<UserConfirmationHistoryDto>(confirmationCodeHistoryDto));

            var service = CreateUserConfirmationHistoryService();

            var response = await service.GetActiveCodeAsync(Guid.NewGuid(), "0000000000").ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserConfirmationHistoryService")]
        public async Task GetActiveCodeAsync_CodeNotFound_ReturnUnsuccessfulResponse()
        {
            Domain.UserConfirmationHistory confirmationCodeHistory = null;

            _ = _repositoryMoq.Setup(s => s.GetFirstOrDefaultAsync(
                    It.IsAny<Expression<Func<Domain.UserConfirmationHistory, bool>>>(),
                    It.IsAny<Func<IQueryable<Domain.UserConfirmationHistory>, IOrderedQueryable<Domain.UserConfirmationHistory>>>(),
                    It.IsAny<Func<IQueryable<Domain.UserConfirmationHistory>, IIncludableQueryable<Domain.UserConfirmationHistory, object>>>(),
                    It.IsAny<bool>())
                ).ReturnsAsync(confirmationCodeHistory);

            _ = _serviceResponseHelperMoq
                .Setup(r => r.SetSuccess(It.IsAny<UserConfirmationHistoryDto>()))
                .Returns(new ServiceResponse<UserConfirmationHistoryDto>(null, new ErrorInfo() { Code = StatusCodes.Status400BadRequest }));

            var service = CreateUserConfirmationHistoryService();

            var response = await service.GetActiveCodeAsync(Guid.NewGuid(), "0000000000").ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(400, response.Error.Code);
        }

        private static UserConfirmationHistoryDto CreateDummyConfirmationHistoryDto(bool isUsed = false)
        {
            var expiredDate = DateTime.UtcNow.AddMinutes(2);

            return new UserConfirmationHistoryDto()
            {
                Code = "123456",
                CodeType = (int)ConfirmationType.PhoneNumber,
                ExpiredDate = expiredDate,
                Id = Guid.Parse("9c15bad3-56d6-4c8e-9b03-5d3201194737"),
                IsUsed = isUsed,
                PhoneNumber = "0000000000",
                UserId = Guid.Parse("00000000-0000-0000-0000-000000000000")
            };
        }

        private static Domain.UserConfirmationHistory CreateDummyConfirmationHistory(bool isUsed = false)
        {
            var expiredDate = DateTime.UtcNow.AddMinutes(2);

            return new Domain.UserConfirmationHistory()
            {
                Code = "123456",
                CodeType = (int)ConfirmationType.PhoneNumber,
                ExpiredDate = expiredDate,
                Id = Guid.Parse("9c15bad3-56d6-4c8e-9b03-5d3201194737"),
                IsUsed = isUsed,
                PhoneNumber = "0000000000",
                UserId = Guid.Parse("00000000-0000-0000-0000-000000000000")
            };
        }

        private UserConfirmationHistoryService CreateUserConfirmationHistoryService()
        {
            var localizationMoq = new Mock<IKsStringLocalizer<UserConfirmationHistoryService>>();
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>(), It.IsAny<object[]>()]).Returns(new LocalizedString("sometext", "sometext"));

            var iKsI18Nmoq = new Mock<IKsI18N>();
            _ = iKsI18Nmoq.Setup(r => r.GetLocalizer<UserConfirmationHistoryService>()).Returns(localizationMoq.Object);
            _ = iKsI18Nmoq.Setup(r => r.UICulture).Returns(CultureInfo.CurrentUICulture);

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IMapper))).Returns(_mapperMoq.Object);
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper))).Returns(_serviceResponseHelperMoq.Object);
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IKsI18N))).Returns(iKsI18Nmoq.Object);

            var service = new UserConfirmationHistoryService(
                userConfirmationRepository: _repositoryMoq.Object,
                mapper: _mapperMoq.Object,
                dataManager: _dataManagerMoq.Object,
                serviceResponseHelper: _serviceResponseHelperMoq.Object,
                i18N: iKsI18Nmoq.Object);

            return service;
        }
    }
}