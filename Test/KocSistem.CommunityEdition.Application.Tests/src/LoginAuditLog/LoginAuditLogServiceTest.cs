﻿// <copyright file="LoginAuditLogTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Excel.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.LoginAuditLog.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.PdfExport.Contracts;
using KocSistem.OneFrame.Data;
using KocSistem.OneFrame.Data.Relational;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Application.Tests.LoginAuditLog
{
    public class LoginAuditLogServiceTest
    {
        private readonly Mock<IConfiguration> _configurationMoq;
        private readonly Mock<IDataManager> _dataManagerMoq;
        private readonly Mock<ILookupNormalizer> _keyNormalizerMoq;
        private readonly Mock<IRepository<Domain.LoginAuditLog>> _loginAuditLogRepositoryMoq;
        private readonly Mock<IMapper> _mapperMoq;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;

        public LoginAuditLogServiceTest()
        {
            _configurationMoq = new Mock<IConfiguration>();
            _dataManagerMoq = new Mock<IDataManager>();
            _keyNormalizerMoq = new Mock<ILookupNormalizer>();
            _loginAuditLogRepositoryMoq = new Mock<IRepository<Domain.LoginAuditLog>>();
            _mapperMoq = new Mock<IMapper>();
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();
        }

        [Fact]
        [Trait("Category", "LoginAuditLogService")]
        public async Task GetLoginAuditLogsAsync_ReturnsSuccessfulResponse()
        {
            var pagedRequest = new PagedRequestDto()
            {
                Orders = new List<PagedRequestOrderDto>(),
                PageIndex = 0,
                PageSize = 10,
            };
            var query = new List<Domain.LoginAuditLog>() { new Domain.LoginAuditLog() }.AsQueryable().BuildMock();
            _ = _loginAuditLogRepositoryMoq.Setup(r => r.GetQueryable(
                  It.IsAny<Expression<Func<Domain.LoginAuditLog, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.LoginAuditLog>, IOrderedQueryable<Domain.LoginAuditLog>>>(),
                  It.IsAny<Func<IQueryable<Domain.LoginAuditLog>, IIncludableQueryable<Domain.LoginAuditLog, object>>>(),
                  It.IsAny<bool>())).Returns(query);

            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<PagedResultDto<LoginAuditLogDto>>())).Returns(new ServiceResponse<PagedResultDto<LoginAuditLogDto>>(new PagedResultDto<LoginAuditLogDto>(), true));
            _ = _mapperMoq.Setup(r => r.Map<IPagedList<Domain.ApplicationSetting>, PagedResultDto<LoginAuditLogDto>>(It.IsAny<IPagedList<Domain.ApplicationSetting>>())).Returns(new PagedResultDto<LoginAuditLogDto>());
            var service = CreateLoginAuditLogService();

            var response = await service.GetLoginAuditLogsAsync(pagedRequest).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "LoginAuditLogService")]
        public async Task Search_Success_ReturnsSuccessfulResponse()
        {
            var searchRequest = new LoginAuditLogSearchDto();
            _ = _keyNormalizerMoq.Setup(r => r.NormalizeName(It.IsAny<string>())).Returns("anykey");
            var query = new List<Domain.LoginAuditLog>() { new Domain.LoginAuditLog() { RequestUserName = "anyname" } }.AsQueryable().BuildMock();
            _ = _loginAuditLogRepositoryMoq.Setup(r => r.GetQueryable(
                  It.IsAny<Expression<Func<Domain.LoginAuditLog, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.LoginAuditLog>, IOrderedQueryable<Domain.LoginAuditLog>>>(),
                  It.IsAny<Func<IQueryable<Domain.LoginAuditLog>, IIncludableQueryable<Domain.LoginAuditLog, object>>>(),
                  It.IsAny<bool>())).Returns(query);

            _ = _mapperMoq.Setup(r => r.Map<IPagedList<Domain.ApplicationSetting>, PagedResultDto<LoginAuditLogDto>>(It.IsAny<IPagedList<Domain.ApplicationSetting>>())).Returns(new PagedResultDto<LoginAuditLogDto>());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<PagedResultDto<LoginAuditLogDto>>())).Returns(new ServiceResponse<PagedResultDto<LoginAuditLogDto>>(new PagedResultDto<LoginAuditLogDto>(), true));
            var service = CreateLoginAuditLogService();

            var response = await service.SearchAsync(searchRequest).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "LoginAuditLogService")]
        public async Task Search_NormalizeNameContainsComma_ReturnsSuccessfulResponse()
        {
            var searchRequest = new LoginAuditLogSearchDto();
            _ = _keyNormalizerMoq.Setup(r => r.NormalizeName(It.IsAny<string>())).Returns("12.01.2001,12.02.2001");
            var pagedResultDto = new PagedResultDto<LoginAuditLogDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                Items = new List<LoginAuditLogDto>(),
            };
            var query = new List<Domain.LoginAuditLog>() { new Domain.LoginAuditLog() { ApplicationUserName = "anyname" } }.AsQueryable().BuildMock();
            _ = _loginAuditLogRepositoryMoq.Setup(r => r.GetQueryable(
                  It.IsAny<Expression<Func<Domain.LoginAuditLog, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.LoginAuditLog>, IOrderedQueryable<Domain.LoginAuditLog>>>(),
                  It.IsAny<Func<IQueryable<Domain.LoginAuditLog>, IIncludableQueryable<Domain.LoginAuditLog, object>>>(),
                  It.IsAny<bool>())).Returns(query);

            _ = _mapperMoq.Setup(r => r.Map<IPagedList<Domain.ApplicationSetting>, PagedResultDto<LoginAuditLogDto>>(It.IsAny<IPagedList<Domain.ApplicationSetting>>())).Returns(new PagedResultDto<LoginAuditLogDto>());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<PagedResultDto<LoginAuditLogDto>>())).Returns(new ServiceResponse<PagedResultDto<LoginAuditLogDto>>(pagedResultDto, true));
            var service = CreateLoginAuditLogService();

            var response = await service.SearchAsync(searchRequest).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
            Assert.Equal(0, response.Result.PageIndex);
            Assert.Equal(10, response.Result.PageSize);
            Assert.IsAssignableFrom<List<LoginAuditLogDto>>(response.Result.Items);
        }

        private LoginAuditLogService CreateLoginAuditLogService(List<Claim> claims = null)
        {
            var localizationMoq = new Mock<IKsStringLocalizer<LoginAuditLogService>>();
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>(), It.IsAny<object[]>()]).Returns(new LocalizedString("sometext", "sometext"));

            var iKsI18Nmoq = new Mock<IKsI18N>();
            _ = iKsI18Nmoq.Setup(r => r.GetLocalizer<LoginAuditLogService>()).Returns(localizationMoq.Object);

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IMapper))).Returns(_mapperMoq.Object);
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper))).Returns(_serviceResponseHelperMoq.Object);
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IKsI18N))).Returns(iKsI18Nmoq.Object);

            var service = new LoginAuditLogService(
                    loginAuditLogRepository: _loginAuditLogRepositoryMoq.Object,
                    mapper: _mapperMoq.Object,
                    dataManager: _dataManagerMoq.Object,
                    serviceResponseHelper: _serviceResponseHelperMoq.Object,
                    i18N: iKsI18Nmoq.Object,
                    keyNormalizer: _keyNormalizerMoq.Object,_configurationMoq.Object);

            if (claims != null)
            {
                var identity = new ClaimsIdentity(claims: claims, authenticationType: "Test");

                var mockPrincipal = new Mock<IPrincipal>();
                _ = mockPrincipal.Setup(expression: x => x.Identity).Returns(value: identity);
                _ = mockPrincipal.Setup(expression: x => x.IsInRole(It.IsAny<string>())).Returns(value: true);
            }

            _ = _configurationMoq.SetupGet(m => m["Identity:Policy:Password:HistoryLimit"]).Returns("10");
            _ = _configurationMoq.SetupGet(m => m["Identity:Policy:Password:ExpireDays"]).Returns("45");
            _ = _configurationMoq.SetupGet(m => m["PdfExport:Encoding"]).Returns("CP1254");

            return service;
        }

        [Fact]
        [Trait("Category", "LoginAuditLogService")]
        public async Task ExcelExport_Success_ReturnsSuccessfulResponse()
        {
            var searchRequest = new LoginAuditLogFilterDto()
            {
                StartDate = DateTime.UtcNow.AddDays(-1).AddSeconds(-1),
                EndDate = DateTime.UtcNow.AddDays(1).AddSeconds(-1)
            };

            var query = new List<Domain.LoginAuditLog>() { new Domain.LoginAuditLog() }.AsQueryable().BuildMock();
            _ = _loginAuditLogRepositoryMoq.Setup(r => r.GetQueryable(
                  It.IsAny<Expression<Func<Domain.LoginAuditLog, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.LoginAuditLog>, IOrderedQueryable<Domain.LoginAuditLog>>>(),
                  It.IsAny<Func<IQueryable<Domain.LoginAuditLog>, IIncludableQueryable<Domain.LoginAuditLog, object>>>(),
                  It.IsAny<bool>())).Returns(query);

            _ = _mapperMoq.Setup(r => r.Map<List<Domain.LoginAuditLog>, List<LoginAuditLogExcelExportDto>>(It.IsAny<List<Domain.LoginAuditLog>>())).Returns(new List<LoginAuditLogExcelExportDto>());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<ExcelExportDto>())).Returns(new ServiceResponse<ExcelExportDto>(null));
            var service = CreateLoginAuditLogService();

            var response = await service.SearchForExcelExportAsync(searchRequest).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "LoginAuditLogService")]
        public async Task PdfExport_Success_ReturnsSuccessfulResponse()
        {
            var searchRequest = new LoginAuditLogFilterDto()
            {
                StartDate = DateTime.UtcNow.AddDays(-1).AddSeconds(-1),
                EndDate = DateTime.UtcNow.AddDays(1).AddSeconds(-1)
            };

            var query = new List<Domain.LoginAuditLog>() { new Domain.LoginAuditLog() }.AsQueryable().BuildMock();
            _ = _loginAuditLogRepositoryMoq.Setup(r => r.GetQueryable(
                  It.IsAny<Expression<Func<Domain.LoginAuditLog, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.LoginAuditLog>, IOrderedQueryable<Domain.LoginAuditLog>>>(),
                  It.IsAny<Func<IQueryable<Domain.LoginAuditLog>, IIncludableQueryable<Domain.LoginAuditLog, object>>>(),
                  It.IsAny<bool>())).Returns(query);

            _ = _mapperMoq.Setup(r => r.Map<List<Domain.LoginAuditLog>, List<LoginAuditLogPdfExport>>(It.IsAny<List<Domain.LoginAuditLog>>())).Returns(new List<LoginAuditLogPdfExport>());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<PdfExportDto>())).Returns(new ServiceResponse<PdfExportDto>(null));
            var service = CreateLoginAuditLogService();

            var response = await service.SearchForPdfExportAsync(searchRequest).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
        }
    }
}