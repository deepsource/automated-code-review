// -----------------------------------------------------------------------
// <copyright file="MenuControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>
// -----------------------------------------------------------------------

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.Menu;
using KocSistem.CommunityEdition.Application.Abstractions.Menu.Contracts;
using KocSistem.CommunityEdition.WebAPI.Controllers;
using KocSistem.CommunityEdition.WebAPI.Model.Menu;
using KocSistem.OneFrame.DesignObjects.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using KocSistem.OneFrame.DesignObjects.Models;
using Xunit;

namespace KocSistem.CommunityEdition.WebAPI.Tests.Controllers
{
    public class MenuControllerTest
    {
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<IMenuService> _menuServiceMock;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;

        public MenuControllerTest()
        {
            _menuServiceMock = new Mock<IMenuService>();
            _mapperMock = new Mock<IMapper>();
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();
        }

        [Fact]
        [Trait("Category", "MenuController")]
        public async Task Get_InvalidMenu_ReturnsKeyNotFoundException()
        {
            var menuDtoList = new List<MenuDto>()
            {
                new MenuDto() { Id = 1, Name = "ParentMenu1", ParentId = null, Url = null },
                new MenuDto() { Id = 2, Name = "ChildMenu1", ParentId = 1, Url = "childmenu1url" },
                new MenuDto() { Id = 4, Name = "ChildMenu2", ParentId = 3, Url = "childmenu2url" },
            };

            var menuResult = new ServiceResponse<IEnumerable<MenuDto>>(menuDtoList);

            var menuModelList = new List<MenuResponse>()
            {
                new MenuResponse() { Id = 1, Name = "ParentMenu", ParentId = null, Url = null },
                new MenuResponse() { Id = 2, Name = "ChildMenu1", ParentId = 1, Url = "childmenu1url" },
                new MenuResponse() { Id = 4, Name = "ChildMenu2", ParentId = 3, Url = "childmenu2url" },
            };

            var claims = new List<Claim>()
            {
                new Claim(JwtRegisteredClaimNames.Sub, Guid.NewGuid().ToString()),
                new Claim("Menu1", "1"),
                new Claim("Menu2", "2"),
                new Claim("Menu4", "4"),
            };

            var identity = new ClaimsIdentity(claims, "TestAuthType");
            var claimsPrincipal = new ClaimsPrincipal(identity);

            var context = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal,
                },
            };

            _ = _menuServiceMock.Setup(mock => mock.GetListAsync(null, null, null)).Returns(Task.FromResult(menuResult));
            _ = _mapperMock.Setup(mock => mock.Map<IList<MenuDto>, List<MenuResponse>>(It.IsAny<List<MenuDto>>())).Returns(menuModelList);

            using var controller = new MenuController(_menuServiceMock.Object, _mapperMock.Object)
            {
                ControllerContext = context,
            };

            var serviceResponse = new ServiceResponse<IList<UserMenuDto>>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo(StatusCodes.Status204NoContent)
            };
            _ = _menuServiceMock.Setup(r => r.GetUserMenuListAsync()).ReturnsAsync(serviceResponse);

            var response = await controller.GetAsync().ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "MenuController")]
        public async Task Get_NonExistingMenu_ReturnsOkResult()
        {
            var menuDtoList = new List<MenuDto>();

            var menuResult = new ServiceResponse<IEnumerable<MenuDto>>(menuDtoList);

            var menuModelList = new List<MenuResponse>();

            var claims = new List<Claim>()
            {
                new Claim(JwtRegisteredClaimNames.Sub, Guid.NewGuid().ToString()),
            };

            var identity = new ClaimsIdentity(claims, "TestAuthType");
            var claimsPrincipal = new ClaimsPrincipal(identity);

            var context = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal,
                },
            };

            _ = _menuServiceMock.Setup(mock => mock.GetListAsync(null, null, null)).Returns(Task.FromResult<ServiceResponse<IEnumerable<MenuDto>>>(menuResult));
            _ = _mapperMock.Setup(mock => mock.Map<IList<UserMenuDto>, IList<MenuResponse>>(It.IsAny<IList<UserMenuDto>>())).Returns(menuModelList);
            _ = _serviceResponseHelperMoq.Setup(i => i.SetSuccess(It.IsAny<IList<MenuResponse>>())).Returns(new ServiceResponse<IList<MenuResponse>>(menuModelList));

            using var controller = new MenuController(_menuServiceMock.Object, _mapperMock.Object)
            {
                ControllerContext = context,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();

            _ = serviceProviderMock
               .Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper)))
               .Returns(_serviceResponseHelperMoq.Object);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            var serviceResponse = new ServiceResponse<IList<UserMenuDto>>(new List<UserMenuDto>())
            {
                IsSuccessful = true,
            };
            _ = _menuServiceMock.Setup(r => r.GetUserMenuListAsync()).ReturnsAsync(serviceResponse);

            var response = await controller.GetAsync().ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "MenuController")]
        public async Task Get_Succeeded_ReturnsOkResult()
        {
            var menuDtoList = new List<UserMenuDto>()
            {
                new UserMenuDto() { Id = 1, Name = "ParentMenu", ParentId = null, Url = null, },
                new UserMenuDto() { Id = 2, Name = "ChildMenu1", ParentId = 1, Url = "childmenu1url" },
                new UserMenuDto() { Id = 3, Name = "ChildMenu2", ParentId = 1, Url = "childmenu2url" },
            };

            var menuResult = new ServiceResponse<IEnumerable<UserMenuDto>>(menuDtoList);

            var menuModelList = new List<MenuResponse>()
            {
                new MenuResponse() { Id = 1, Name = "ParentMenu", ParentId = null, Url = null },
                new MenuResponse() { Id = 2, Name = "ChildMenu1", ParentId = 1, Url = "childmenu1url" },
                new MenuResponse() { Id = 3, Name = "ChildMenu2", ParentId = 1, Url = "childmenu2url" },
            };

            var claims = new List<Claim>()
            {
                new Claim("testClaimType", "Menu1"),
                new Claim("testClaimType", "Menu1"),
            };

            var identity = new ClaimsIdentity(claims, "TestAuthType");
            var claimsPrincipal = new ClaimsPrincipal(identity);

            var context = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = claimsPrincipal,
                },
            };

            _ = _mapperMock.Setup(mock => mock.Map<IList<UserMenuDto>, IList<MenuResponse>>(It.IsAny<IList<UserMenuDto>>())).Returns(menuModelList);
            _ = _serviceResponseHelperMoq.Setup(i => i.SetSuccess(It.IsAny<IList<MenuResponse>>())).Returns(new ServiceResponse<IList<MenuResponse>>(menuModelList));

            using var controller = new MenuController(_menuServiceMock.Object, _mapperMock.Object)
            {
                ControllerContext = context,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();

            _ = serviceProviderMock
               .Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper)))
               .Returns(_serviceResponseHelperMoq.Object);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            var serviceResponse = new ServiceResponse<IList<UserMenuDto>>(menuDtoList)
            {
                IsSuccessful = true,
            };
            _ = _menuServiceMock.Setup(r => r.GetUserMenuListAsync()).ReturnsAsync(serviceResponse);

            var response = await controller.GetAsync().ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = ((ServiceResponse<IList<MenuResponse>>)result.Value).Result;

            _ = Assert.IsAssignableFrom<IList<MenuResponse>>(resultResponse);

            Assert.True(resultResponse.Count(m => m.ParentId == null) == 1);

            var root = resultResponse.First(m => m.ParentId == null);
            Assert.True(root.Children.Count == 0);

            Assert.True(root.Children.Distinct().Count() == root.Children.Count);
        }

        [Fact]
        [Trait("Category", "MenuController")]
        public async Task GetMenuTree_NotFound_ReturnsNotFound()
        {
            var serviceResponse = new ServiceResponse<List<MenuTreeViewItemDto>>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _menuServiceMock.Setup(r => r.GetMenuTreeAsync()).ReturnsAsync(serviceResponse);

            using var controller = CreateMenuController();
            var response = await controller.GetMenuTreeAsync().ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "MenuController")]
        public async Task GetMenuTree_Success_ReturnsOkObject()
        {
            using var controller = CreateMenuController();
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<List<MenuTreeViewItem>>())).Returns(new ServiceResponse<List<MenuTreeViewItem>>(new List<MenuTreeViewItem>()));

            var serviceResponse = new ServiceResponse<List<MenuTreeViewItemDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _menuServiceMock.Setup(r => r.GetMenuTreeAsync()).ReturnsAsync(serviceResponse);

            var response = await controller.GetMenuTreeAsync().ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "MenuController")]
        public async Task SaveMenuOrder_NotFound_ReturnsNotFound()
        {
            var req = new SaveMenuOrderModel();

            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _menuServiceMock.Setup(r => r.SaveMenuOrderAsync(It.IsAny<SaveMenuOrderDto>())).ReturnsAsync(serviceResponse);

            _ = _serviceResponseHelperMoq.Setup(s => s.SetError(It.IsAny<string>(), StatusCodes.Status204NoContent, true)).Returns(new ServiceResponse(false));
            using var controller = CreateMenuController();
            var response = await controller.SaveMenuOrderAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "MenuController")]
        public async Task SaveMenuOrder_ReturnsOkObjectResult()
        {
            var menuList = new List<SaveMenuOrderItemModel>()
            {
                new SaveMenuOrderItemModel()
                {
                    Id= 1,
                    ParentId = null,
                    OrderId = 0,
                },
                new SaveMenuOrderItemModel()
                {
                    Id = 2,
                    ParentId = 1,
                    OrderId = 1,
                },
            };

            var req = new SaveMenuOrderModel() { MenuList = menuList };

            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = true,
            };
            _ = _menuServiceMock.Setup(r => r.SaveMenuOrderAsync(It.IsAny<SaveMenuOrderDto>())).ReturnsAsync(serviceResponse);

            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess()).Returns(new ServiceResponse());
            using var controller = CreateMenuController();

            var response = await controller.SaveMenuOrderAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        private MenuController CreateMenuController()
        {
            var controller = new MenuController(_menuServiceMock.Object, _mapperMock.Object);

            var request = new Mock<HttpRequest>();
            _ = request.Setup(expression: x => x.Scheme).Returns(value: "http");

            var httpContext = Mock.Of<HttpContext>(predicate: c => c.Request == request.Object);

            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = httpContext,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();

            _ = serviceProviderMock
                .Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper)))
                .Returns(_serviceResponseHelperMoq.Object);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            return controller;
        }
    }
}