﻿// <copyright file="ApplicationSettingCategoryControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.ApplicationSettingCategory;
using KocSistem.CommunityEdition.Application.Abstractions.ApplicationSettingCategory.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.CommunityEdition.WebAPI.Controllers;
using KocSistem.CommunityEdition.WebAPI.Model.ApplicationSettingCategory;
using KocSistem.CommunityEdition.WebAPI.Model.Paging;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.WebAPI.Tests.Controllers
{
    public class ApplicationSettingCategoryControllerTest
    {
        private readonly Mock<IKsI18N> _localizationMock;
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<IApplicationSettingCategoryService> _applicationSettingCategoryServiceMock;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMock;

        public ApplicationSettingCategoryControllerTest()
        {
            _localizationMock = new Mock<IKsI18N>();
            _applicationSettingCategoryServiceMock = new Mock<IApplicationSettingCategoryService>();
            _mapperMock = new Mock<IMapper>();
            _serviceResponseHelperMock = new Mock<IServiceResponseHelper>();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController")]
        public async Task Get_AllApplicationSettingCategories_ReturnsOkObjectResultForEmptyList()
        {
            var pagedResult = new PagedResult<ApplicationSettingCategoryResponse>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<ApplicationSettingCategoryResponse>() { new ApplicationSettingCategoryResponse() { Name = "category2", Description = "category2", Id = Guid.NewGuid() }, new ApplicationSettingCategoryResponse() { Name = "category1", Description = "category1", Id = Guid.NewGuid() } },
            };
            var pagedResultDto = new PagedResultDto<ApplicationSettingCategoryDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<ApplicationSettingCategoryDto>() { new ApplicationSettingCategoryDto() { Name = "category2", Description = "category2", Id = Guid.NewGuid() }, new ApplicationSettingCategoryDto() { Name = "category1", Description = "category1", Id = Guid.NewGuid() } },
            };
            var req = new PagedRequestDto()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { ColumnName = "Name", DirectionDesc = true } },
            };

            var req1 = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Name", DirectionDesc = true } },
            };

            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<PagedResult<ApplicationSettingCategoryResponse>>())).Returns(new ServiceResponse<PagedResult<ApplicationSettingCategoryResponse>>(pagedResult));
            _ = _mapperMock.Setup(s => s.Map<List<PagedRequestOrder>, List<PagedRequestOrderDto>>(It.IsAny<List<PagedRequestOrder>>())).Returns(req.Orders);
            _ = _mapperMock.Setup(s => s.Map<PagedRequest, PagedRequestDto>(It.IsAny<PagedRequest>())).Returns(req);

            _ = _localizationMock.SetupGet(x => x.GetLocalizer<ApplicationSettingCategoryController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));

            var responsePagedResultDto = new ServiceResponse<PagedResultDto<ApplicationSettingCategoryDto>>(pagedResultDto);

            _ = _applicationSettingCategoryServiceMock.Setup(r => r.GetApplicationSettingCategoryListAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(responsePagedResultDto);

            var controller = CreateApplicationSettingCategoryController();

            var response = await controller.GetAsync(req1).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController")]
        public async Task Get_AllApplicationSettingCategories_ReturnsOkResult()
        {
            var pagedResult = new PagedResult<ApplicationSettingCategoryResponse>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<ApplicationSettingCategoryResponse>() { new ApplicationSettingCategoryResponse() { Name = "category2", Description = "category2" }, new ApplicationSettingCategoryResponse() { Name = "category1", Description = "category1" } },
            };
            var pagedResultDto = new PagedResultDto<ApplicationSettingCategoryDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<ApplicationSettingCategoryDto>() { new ApplicationSettingCategoryDto() { Name = "category2" }, new ApplicationSettingCategoryDto() { Name = "category1" } },
            };
            var req = new PagedRequestDto()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { ColumnName = "Name", DirectionDesc = true } },
            };

            var req1 = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Name", DirectionDesc = true } },
            };

            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<PagedResult<ApplicationSettingCategoryResponse>>())).Returns(new ServiceResponse<PagedResult<ApplicationSettingCategoryResponse>>(pagedResult));
            _ = _mapperMock.Setup(s => s.Map<List<PagedRequestOrder>, List<PagedRequestOrderDto>>(It.IsAny<List<PagedRequestOrder>>())).Returns(req.Orders);
            _ = _mapperMock.Setup(s => s.Map<PagedRequest, PagedRequestDto>(It.IsAny<PagedRequest>())).Returns(req);

            _ = _localizationMock.SetupGet(x => x.GetLocalizer<ApplicationSettingCategoryController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));

            var responsePagedResultDto = new ServiceResponse<PagedResultDto<ApplicationSettingCategoryDto>>(pagedResultDto);

            _ = _applicationSettingCategoryServiceMock.Setup(r => r.GetApplicationSettingCategoryListAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(responsePagedResultDto);

            var controller = CreateApplicationSettingCategoryController();

            var response = await controller.GetAsync(req1).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController")]
        public async Task Get_AllApplicationSettingCategories_ReturnsBadRequestObjectResult()
        {
            var pagedResult = new PagedResult<ApplicationSettingCategoryResponse>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<ApplicationSettingCategoryResponse>() { new ApplicationSettingCategoryResponse() { Name = "category2", Description = "category2" }, new ApplicationSettingCategoryResponse() { Name = "category1", Description = "category1" } },
            };
            var pagedResultDto = new PagedResultDto<ApplicationSettingCategoryDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<ApplicationSettingCategoryDto>() { new ApplicationSettingCategoryDto() { Name = "category2" }, new ApplicationSettingCategoryDto() { Name = "category1" } },
            };
            var req = new PagedRequestDto()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { ColumnName = "Name", DirectionDesc = true } },
            };

            var req1 = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Name", DirectionDesc = true } },
            };

            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<PagedResult<ApplicationSettingCategoryResponse>>(), It.IsAny<string>(), StatusCodes.Status400BadRequest, true)).Returns(new ServiceResponse<PagedResult<ApplicationSettingCategoryResponse>>(pagedResult, false));
            _ = _mapperMock.Setup(s => s.Map<List<PagedRequestOrder>, List<PagedRequestOrderDto>>(It.IsAny<List<PagedRequestOrder>>())).Returns(req.Orders);
            _ = _mapperMock.Setup(s => s.Map<PagedRequest, PagedRequestDto>(It.IsAny<PagedRequest>())).Returns(req);

            _ = _localizationMock.SetupGet(x => x.GetLocalizer<ApplicationSettingController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));

            var serviceResponse = new ServiceResponse<PagedResultDto<ApplicationSettingCategoryDto>>(pagedResultDto, new ErrorInfo(StatusCodes.Status204NoContent), false);

            _ = _applicationSettingCategoryServiceMock.Setup(r => r.GetApplicationSettingCategoryListAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(serviceResponse);

            var controller = CreateApplicationSettingCategoryController();

            var response = await controller.GetAsync(req1).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController")]
        public async Task GetById_ReturnsNoContentResult()
        {
            var applicationSettingCategoryDto = new ApplicationSettingCategoryDto
            {
                Id = Guid.NewGuid(),
            };

            var resp = new ServiceResponse<ApplicationSettingCategoryDto>(applicationSettingCategoryDto)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _applicationSettingCategoryServiceMock.Setup(r => r.GetApplicationSettingCategoryByIdAsync(It.IsAny<Guid>())).ReturnsAsync(resp);

            var controller = CreateApplicationSettingCategoryController();

            var response = await controller.GetByIdAsync(applicationSettingCategoryDto.Id).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController")]
        public async Task GetById_ReturnsOkObjectResult()
        {
            var applicationSettingCategoryDto = new ApplicationSettingCategoryDto()
            {
                Id = Guid.NewGuid(),
            };

            var resp = new ServiceResponse<ApplicationSettingCategoryDto>(applicationSettingCategoryDto) { IsSuccessful = true };
            _ = _applicationSettingCategoryServiceMock.Setup(r => r.GetApplicationSettingCategoryByIdAsync(It.IsAny<Guid>())).ReturnsAsync(resp);
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<ApplicationSettingCategoryResponse>())).Returns(new ServiceResponse<ApplicationSettingCategoryResponse>(new ApplicationSettingCategoryResponse()));

            var controller = CreateApplicationSettingCategoryController();

            var response = await controller.GetByIdAsync(applicationSettingCategoryDto.Id).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController")]
        public async Task Post_CreateApplicationSettingCategory_ReturnsBadRequestObjectResult()
        {
            List<ApplicationSettingCategoryDto> applicationSettingCategoryDtos = null;
            var resp = new ServiceResponse<ApplicationSettingCategoryDto>(result: null, isSuccessful: false);
            _ = _applicationSettingCategoryServiceMock.Setup(m => m.CreateApplicationSettingCategoryAsync(null)).Returns(Task.FromResult(resp));
            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(resp);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<ApplicationSettingCategoryController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = _mapperMock.Setup(mock => mock.Map<IEnumerable<ApplicationSettingCategoryPostRequest>, List<ApplicationSettingCategoryDto>>(It.IsAny<IEnumerable<ApplicationSettingCategoryPostRequest>>())).Returns(applicationSettingCategoryDtos);

            var controller = CreateApplicationSettingCategoryController();

            var response = await controller.PostAsync(null).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController")]
        public async Task Post_CreateApplicationSettingCategory_ReturnsOkResult()
        {
            var applicationSettingCategoryName = "Category 1";
            var applicationSettingCategoryPostRequest = new ApplicationSettingCategoryPostRequest { Name = applicationSettingCategoryName };
            var dto = _mapperMock.Object.Map<ApplicationSettingCategoryPostRequest, ApplicationSettingCategoryDto>(applicationSettingCategoryPostRequest);
            _ = _applicationSettingCategoryServiceMock.Setup(m => m.CreateApplicationSettingCategoryAsync(dto)).Returns(Task.FromResult(new ServiceResponse<ApplicationSettingCategoryDto>(result: dto)));
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<ApplicationSettingCategoryResponse>())).Returns(new ServiceResponse<ApplicationSettingCategoryResponse>(new ApplicationSettingCategoryResponse()));
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess()).Returns(new ServiceResponse());

            var controller = CreateApplicationSettingCategoryController();

            var response = await controller.PostAsync(applicationSettingCategoryPostRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController")]
        public async Task Put_CreateApplicationSettingCategory_ReturnsBadRequestObjectResult()
        {
            List<ApplicationSettingCategoryDto> applicationSettingCategoryDtos = null;
            var resp = new ServiceResponse<ApplicationSettingCategoryDto>(null) { Result = null, IsSuccessful = false };
            _ = _applicationSettingCategoryServiceMock.Setup(m => m.UpdateApplicationSettingCategoryAsync(null)).Returns(Task.FromResult(resp));
            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(resp);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<ApplicationSettingCategoryController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = _mapperMock.Setup(mock => mock.Map<IEnumerable<ApplicationSettingCategoryPutRequest>, List<ApplicationSettingCategoryDto>>(It.IsAny<IEnumerable<ApplicationSettingCategoryPutRequest>>())).Returns(applicationSettingCategoryDtos);

            var controller = CreateApplicationSettingCategoryController();

            var response = await controller.PutAsync(null).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController")]
        public async Task Put_CreateApplicationSettingCategory_ReturnsOkResult()
        {
            var applicationSettingCategoryName = "Category 1";
            var applicationSettingCategoryPutRequest = new ApplicationSettingCategoryPutRequest { Id = Guid.NewGuid(), Name = applicationSettingCategoryName };
            _ = _applicationSettingCategoryServiceMock.Setup(m => m.UpdateApplicationSettingCategoryAsync(_mapperMock.Object.Map<ApplicationSettingCategoryPutRequest, ApplicationSettingCategoryDto>(applicationSettingCategoryPutRequest))).Returns(Task.FromResult(new ServiceResponse<ApplicationSettingCategoryDto>(null)));
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<ApplicationSettingCategoryResponse>())).Returns(new ServiceResponse<ApplicationSettingCategoryResponse>(new ApplicationSettingCategoryResponse()));
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<ApplicationSettingCategoryController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));

            var controller = CreateApplicationSettingCategoryController();

            var response = await controller.PutAsync(applicationSettingCategoryPutRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController")]
        public async Task Delete_CreateApplicationSettingCategory_ReturnsNoContentResult()
        {
            var resp = new ServiceResponse() { IsSuccessful = false, Error = new ErrorInfo(StatusCodes.Status204NoContent) };
            _ = _applicationSettingCategoryServiceMock.Setup(m => m.DeleteApplicationSettingCategoryAsync(It.IsAny<Guid>())).Returns(Task.FromResult(resp));
            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(resp);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<ApplicationSettingCategoryController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));

            var controller = CreateApplicationSettingCategoryController();
            var response = await controller.DeleteAsync(Guid.NewGuid()).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController")]
        public async Task Delete_CreateApplicationSettingCategory_ReturnsBadRequestObjectResult()
        {
            var resp = new ServiceResponse() { IsSuccessful = false, Error = new ErrorInfo(StatusCodes.Status400BadRequest) };
            _ = _applicationSettingCategoryServiceMock.Setup(m => m.DeleteApplicationSettingCategoryAsync(It.IsAny<Guid>())).Returns(Task.FromResult(resp));
            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(resp);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<ApplicationSettingCategoryController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));

            var controller = CreateApplicationSettingCategoryController();
            var response = await controller.DeleteAsync(Guid.NewGuid()).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController")]
        public async Task Delete_CreateApplicationSettingCategory_ReturnsOkResult()
        {
            _ = _applicationSettingCategoryServiceMock.Setup(m => m.DeleteApplicationSettingCategoryAsync(It.IsAny<Guid>())).Returns(Task.FromResult(new ServiceResponse()));
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess()).Returns(new ServiceResponse());
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<ApplicationSettingCategoryController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));

            var controller = CreateApplicationSettingCategoryController();

            var response = await controller.DeleteAsync(Guid.NewGuid()).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController")]
        public async Task Search_Success_ReturnsOkObjectResult()
        {
            var req = new ApplicationSettingCategorySearchRequest()
            {
                PageIndex = 10,
                Name = "anyname",
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Key", DirectionDesc = true } },
            };

            var res = new ApplicationSettingCategoryResponse();
            var pagedResult = new PagedResult<ApplicationSettingCategoryResponse>()
            {
                Items = new List<ApplicationSettingCategoryResponse>() { res },
            };

            _ = _mapperMock.Setup(r => r.Map<PagedResultDto<ApplicationSettingCategoryDto>, PagedResult<ApplicationSettingCategoryResponse>>(It.IsAny<PagedResultDto<ApplicationSettingCategoryDto>>())).Returns(pagedResult);
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(pagedResult)).Returns(new ServiceResponse<PagedResult<ApplicationSettingCategoryResponse>>(pagedResult));

            var serviceResponse = new ServiceResponse<PagedResultDto<ApplicationSettingCategoryDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _applicationSettingCategoryServiceMock.Setup(s => s.SearchAsync(It.IsAny<ApplicationSettingCategorySearchDto>())).ReturnsAsync(serviceResponse);

            var controller = CreateApplicationSettingCategoryController();

            var response = await controller.SearchAsync(req).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        private ApplicationSettingCategoryController CreateApplicationSettingCategoryController()
        {
            var controller = new ApplicationSettingCategoryController(_applicationSettingCategoryServiceMock.Object, _mapperMock.Object);

            var request = new Mock<HttpRequest>();
            _ = request.Setup(expression: x => x.Scheme).Returns(value: "http");

            var httpContext = Mock.Of<HttpContext>(predicate: c => c.Request == request.Object);

            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = httpContext,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();

            _ = serviceProviderMock
               .Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper)))
               .Returns(_serviceResponseHelperMock.Object);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            return controller;
        }
    }
}