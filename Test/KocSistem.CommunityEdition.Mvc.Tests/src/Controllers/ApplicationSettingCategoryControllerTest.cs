﻿// <copyright file="ApplicationSettingCategoryControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Infrastructure.Helpers.Client;
using KocSistem.CommunityEdition.Mvc.Controllers;
using KocSistem.CommunityEdition.Mvc.Models.ApplicationSettingCategory;
using KocSistem.CommunityEdition.Mvc.Models.DataTables;
using KocSistem.CommunityEdition.Mvc.Models.Paging;
using KocSistem.CommunityEdition.Mvc.Tests.Helpers;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Localization;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Mvc.Tests.Controllers
{
    public class ApplicationSettingCategoryControllerTest
    {
        private readonly ErrorInfo _errorInfo = new("sometext");
        private readonly Mock<IHttpClientFactory> _httpClientFactoryMock;
        private readonly Mock<IKsI18N> _localizationMoq;
        private readonly Mock<IClientProxy> _proxyHelperMock;
        private readonly string _sampleResponse = "{\"pageIndex\":0,\"pageSize\":1,\"totalCount\":2,\"totalPages\":1,\"items\":[{\"id\":\"2d27aa6e-ba08-4468-8614-280351e7d642\",\"name\":\"Admin\",\"description\":\"Admin desc\"}]}";

        public ApplicationSettingCategoryControllerTest()
        {
            _httpClientFactoryMock = new Mock<IHttpClientFactory>();
            _proxyHelperMock = new Mock<IClientProxy>();
            _localizationMoq = new Mock<IKsI18N>();
            _ = _localizationMoq.SetupGet(x => x.GetLocalizer<ApplicationSettingCategoryController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public void Create_Success_ReturnsViewResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = controller.Create();

            _ = Assert.IsAssignableFrom<ViewResult>(result);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task Delete_NotFound_ReturnToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = await controller.DeleteAsync(Guid.NewGuid()).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task Delete_OkAPIResponse_ReturnsToastSuccessForRedirect()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = await controller.DeleteAsync(Guid.NewGuid()).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task Delete_UnaouthorizedAPIResponse_ReturnToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = await controller.DeleteAsync(Guid.NewGuid()).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public void Get_ReturnsViewResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse(true);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = controller.Get();

            _ = Assert.IsAssignableFrom<ViewResult>(result);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task GetById_NotFound_ReturnToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse<ApplicationSettingCategoryViewModel>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = await controller.GetAppSettingCategoryInfoAsync(Guid.NewGuid()).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task GetById_Success_ReturnViewResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<ApplicationSettingCategoryViewModel>(new ApplicationSettingCategoryViewModel());
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = await controller.GetAppSettingCategoryInfoAsync(Guid.NewGuid()).ConfigureAwait(false);

            Assert.IsAssignableFrom<PartialViewResult>(result);
            var response = (PartialViewResult)result;
            Assert.IsAssignableFrom<ApplicationSettingCategoryViewModel>(response.Model);
            var model = (ApplicationSettingCategoryViewModel)response.Model;
            Assert.Equal(data.Result.Id, model.Id);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task GetById_UnaouthorizedAPIResponse_ReturnToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse<ApplicationSettingCategoryViewModel>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = await controller.GetAppSettingCategoryInfoAsync(Guid.NewGuid()).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task GetList_NotFound_ReturnToastError()
        {
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, null, null, null);

            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse<PagedResult<ApiAppSettingCategoryGetResponseModel>>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task GetList_RequestModelSearchNotNull_ReturnOkObjectResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<PagedResult<ApiAppSettingCategoryGetResponseModel>>(new PagedResult<ApiAppSettingCategoryGetResponseModel>());
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);
            var search = new Search("anyString", true);
            var order = new Order(0, "anyString");
            var dataColumn = new DataTableColumn("data", "name", true, true, "name", true);
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, search, new List<Order>() { order }, new List<DataTableColumn>() { dataColumn });

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
            Assert.Equal("name", dataColumn.Name);
            Assert.True(dataColumn.Searchable);
            Assert.True(dataColumn.Orderable);
            Assert.Equal("name", dataColumn.SearchValue);
            Assert.True(dataColumn.SearchRegEx);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task GetList_Success_ReturnOkObjectResult()
        {
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, null, null, null);
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<PagedResult<ApiAppSettingCategoryGetResponseModel>>(new PagedResult<ApiAppSettingCategoryGetResponseModel>());
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task GetList_UnaouthorizedAPIResponse_ReturnToastError()
        {
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, null, null, null);
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse<PagedResult<ApiAppSettingCategoryGetResponseModel>>(new PagedResult<ApiAppSettingCategoryGetResponseModel>(), _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task Post_NotFound_ReturnToastError()
        {
            var model = new ApplicationSettingCategoryPostModel() { Name = "anyname", Description = "anydescription" };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = await controller.PostAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task Post_OkAPIResponse_ReturnsToastSuccessForRedirect()
        {
            var model = new ApplicationSettingCategoryPostModel() { Name = "anyname", Description = "anydescription" };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = await controller.PostAsync(model).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task Post_UnaouthorizedAPIResponse_ReturnToastError()
        {
            var model = new ApplicationSettingCategoryPostModel() { Name = "anyname", Description = "anydescription" };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = await controller.PostAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task Put_NotFound_ReturnToastError()
        {
            var model = new AppSettingCategoryPutViewModel() { Name = "anyname", Id = Guid.NewGuid(), Description = "anydescription" };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = await controller.PutAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task Put_OkAPIResponse_ReturnsToastSuccessForRedirect()
        {
            var model = new AppSettingCategoryPutViewModel() { Name = "anyname", Id = Guid.NewGuid(), Description = "anydescription" };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = await controller.PutAsync(model).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task Put_UnaouthorizedAPIResponse_ReturnToastError()
        {
            var model = new AppSettingCategoryPutViewModel() { Name = "anyname", Id = Guid.NewGuid(), Description = "anydescription" };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = await controller.PutAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task Search_NotFound_ReturnToastError()
        {
            var model = new ApplicationSettingCategorySearchRequest() { Name = "anyname", Orders = new List<PagedRequestOrder>(), PageIndex = 1, PageSize = 2 };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse<PagedResult<ApiAppSettingCategoryGetResponseModel>>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = await controller.SearchAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task Search_Success_ReturnOkObjectResult()
        {
            var model = new ApplicationSettingCategorySearchRequest() { Name = "anyname", Orders = new List<PagedRequestOrder>(), PageIndex = 1, PageSize = 2 };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<PagedResult<ApiAppSettingCategoryGetResponseModel>>(new PagedResult<ApiAppSettingCategoryGetResponseModel>());
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = await controller.SearchAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryController-MVC")]
        public async Task Search_UnaouthorizedAPIResponse_ReturnToastError()
        {
            var model = new ApplicationSettingCategorySearchRequest() { Name = "anyname", Orders = new List<PagedRequestOrder>(), PageIndex = 1, PageSize = 2 };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse<PagedResult<ApiAppSettingCategoryGetResponseModel>>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateApplicationSettingCategoryController(httpClientFactory, proxyHelper);

            var result = await controller.SearchAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        private static void AssertToastError(JsonResult toastResult)
        {
            var resultValue = JObject.Parse(JsonConvert.SerializeObject(toastResult.Value));
            Assert.Equal("Error", resultValue["Type"].Value<string>());
            Assert.StartsWith("sometext", resultValue["Message"].Value<string>(), StringComparison.CurrentCulture);
            Assert.Equal("False", resultValue["IsRedirect"].Value<string>());
        }

        private ApplicationSettingCategoryController CreateApplicationSettingCategoryController(IHttpClientFactory httpClientFactory, IClientProxy proxyHelper, IUrlHelperFactory urlHelperFactory = null, List<Claim> claims = null)
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());

            var controller = new ApplicationSettingCategoryController(_localizationMoq.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext,
                },
                TempData = tempData,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock
                .Setup(serviceProvider => serviceProvider.GetService(typeof(IHttpClientFactory)))
                .Returns(httpClientFactory);

            _ = serviceProviderMock
             .Setup(serviceProvider => serviceProvider.GetService(typeof(IClientProxy)))
             .Returns(proxyHelper);

            if (urlHelperFactory != null)
            {
                _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IUrlHelperFactory))).Returns(urlHelperFactory);
            }

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            if (claims != null)
            {
                var identity = new ClaimsIdentity(claims: claims, authenticationType: "Test");
                var claimsPrincipal = new ClaimsPrincipal(identity: identity);
                var mockPrincipal = new Mock<IPrincipal>();
                _ = mockPrincipal.Setup(expression: x => x.Identity).Returns(value: identity);
                _ = mockPrincipal.Setup(expression: x => x.IsInRole(It.IsAny<string>())).Returns(value: true);

                controller.ControllerContext.HttpContext.User = claimsPrincipal;
            }

            return controller;
        }
    }
}