﻿// <copyright file="HomeControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Infrastructure.Helpers.Client;
using KocSistem.CommunityEdition.Mvc.Controllers;
using KocSistem.CommunityEdition.Mvc.Models.Home;
using KocSistem.CommunityEdition.Mvc.Models.Profile;
using KocSistem.CommunityEdition.Mvc.Tests.Helpers;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Mvc.Tests.Controllers
{
    public class HomeControllerTest
    {
        private readonly Mock<IKsI18N> _localizationMoq;
        private readonly Mock<IConfiguration> _configurationMoq;
        private readonly Mock<IClientProxy> _proxyHelperMock;
        private readonly Mock<IHttpClientFactory> _httpClientFactoryMock;
        private readonly ErrorInfo _errorInfo = new("sometext");
        private readonly string _sampleResponse = "{\"pageIndex\":0,\"pageSize\":1,\"totalCount\":2,\"totalPages\":1,\"items\":[{\"id\":\"2d27aa6e-ba08-4468-8614-280351e7d642\",\"name\":\"Admin\",\"description\":\"Admin desc\"}]}";

        public HomeControllerTest()
        {
            _localizationMoq = new Mock<IKsI18N>();
            _ = _localizationMoq.SetupGet(x => x.GetLocalizer<HomeController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _proxyHelperMock = new Mock<IClientProxy>();
            _httpClientFactoryMock = new Mock<IHttpClientFactory>();
            _configurationMoq = new Mock<IConfiguration>();
        }

        [Fact]
        [Trait("Category", "HomeController-MVC")]
        public void CheckThemeName_Success_ReturnsString()
        {
            var result = HomeController.CheckThemeName(null);

            Assert.Equal("dark", result);
        }

        [Fact]
        [Trait("Category", "HomeController-MVC")]
        public async Task Index_NotFound_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse<ProfileModel>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateHomeController(httpClientFactory, proxyHelper);

            var result = await controller.IndexAsync().ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "HomeController-MVC")]
        public async Task Index_Success_ReturnsViewModel()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<ProfileModel>(new ProfileModel() { Email = "test@test.com", Name = "John" });
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateHomeController(httpClientFactory, proxyHelper);

            var result = await controller.IndexAsync().ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<ViewResult>(result);
            var viewResult = (ViewResult)result;
            var resultItem = (IndexModel)viewResult.Model;
            Assert.Equal("John", resultItem.Profile.Name);
        }

        [Fact]
        [Trait("Category", "HomeController-MVC")]
        public void SetLanguage_Success_ReturnsLocalRedirectResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse<ProfileModel>(null, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateHomeController(httpClientFactory, proxyHelper);

            var result = controller.SetLanguage("tr-TR", "anyString");

            Assert.IsAssignableFrom<RedirectResult>(result);
            var response = (RedirectResult)result;
            Assert.Equal("anyString", response.Url);
        }

        [Fact]
        [Trait("Category", "HomeController-MVC")]
        public void SetTheme_Success_ReturnsLocalRedirectResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse<ProfileModel>(null, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateHomeController(httpClientFactory, proxyHelper);

            var result = controller.SetTheme("dark", "anyString");

            Assert.IsAssignableFrom<RedirectResult>(result);
            var response = (RedirectResult)result;
            Assert.Equal("anyString", response.Url);
        }

        private static void AssertToastError(JsonResult toastResult)
        {
            var resultValue = JObject.Parse(JsonConvert.SerializeObject(toastResult.Value));
            Assert.Equal("Error", resultValue["Type"].Value<string>());
            Assert.StartsWith("sometext", resultValue["Message"].Value<string>(), StringComparison.CurrentCulture);
            Assert.Equal("False", resultValue["IsRedirect"].Value<string>());
        }

        private HomeController CreateHomeController(IHttpClientFactory httpClientFactory, IClientProxy proxyHelper)
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());

            var controller = new HomeController(_localizationMoq.Object, _configurationMoq.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext,
                },
                TempData = tempData,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock
                .Setup(serviceProvider => serviceProvider.GetService(typeof(IHttpClientFactory)))
                .Returns(httpClientFactory);

            _ = serviceProviderMock
             .Setup(serviceProvider => serviceProvider.GetService(typeof(IClientProxy)))
             .Returns(proxyHelper);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            return controller;
        }
    }
}