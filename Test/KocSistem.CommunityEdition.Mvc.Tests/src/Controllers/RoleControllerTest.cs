﻿// <copyright file="RoleControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Common.Enums;
using KocSistem.CommunityEdition.Infrastructure.Helpers.Client;
using KocSistem.CommunityEdition.Mvc.Controllers;
using KocSistem.CommunityEdition.Mvc.Models.ClaimHelper;
using KocSistem.CommunityEdition.Mvc.Models.DataTables;
using KocSistem.CommunityEdition.Mvc.Models.Other;
using KocSistem.CommunityEdition.Mvc.Models.Paging;
using KocSistem.CommunityEdition.Mvc.Models.Role;
using KocSistem.CommunityEdition.Mvc.Models.User;
using KocSistem.CommunityEdition.Mvc.Tests.Helpers;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Localization;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Mvc.Tests.Controllers
{
    public class RoleControllerTest
    {
        private readonly ErrorInfo _errorInfo = new("sometext");
        private readonly Mock<IHttpClientFactory> _httpClientFactoryMock;
        private readonly Mock<IKsI18N> _localizationMock;
        private readonly Mock<IClientProxy> _proxyHelperMock;
        private readonly string _sampleResponse = "{\"pageIndex\":0,\"pageSize\":1,\"totalCount\":2,\"totalPages\":1,\"items\":[{\"id\":\"2d27aa6e-ba08-4468-8614-280351e7d642\",\"name\":\"Admin\",\"description\":\"Admin desc\"}]}";

        public RoleControllerTest()
        {
            _localizationMock = new Mock<IKsI18N>();
            _httpClientFactoryMock = new Mock<IHttpClientFactory>();
            _proxyHelperMock = new Mock<IClientProxy>();
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public void Create_Success_ReturnsViewResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = controller.Create();

            _ = Assert.IsAssignableFrom<ViewResult>(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task Delete_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));

            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.DeleteAsync("Admin").ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task Delete_OkAPIResponse_ReturnsToastSuccess()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.DeleteAsync("Admin").ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task Delete_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(_errorInfo);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.DeleteAsync("Admin").ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task Get_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<PagedResult<ApiRoleGetResponseModel>>(null, _errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);
            var dataTablesRequest = CreateDataTablesRequest();

            var result = await controller.GetAsync(dataTablesRequest).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task Get_OkAPIResponse_ReturnsOK()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<PagedResult<ApiRoleGetResponseModel>>(new PagedResult<ApiRoleGetResponseModel>(), true);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);
            var dataTablesRequest = CreateDataTablesRequest();

            var result = await controller.GetAsync(dataTablesRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task Get_RequestModelSearchNotNull_ReturnsOk()
        {
            var res = new ApiRoleGetResponseModel()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "anyName",
                Description = "Desc",
            };
            var pagedResult = new PagedResult<ApiRoleGetResponseModel>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalCount = 1,
                TotalPages = 1,
                Items = new List<ApiRoleGetResponseModel>() { res },
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<PagedResult<ApiRoleGetResponseModel>>(pagedResult);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);
            var search = new Search("anyString", true);
            var order = new Order(0, "anyString");
            var dataColumn = new DataTableColumn("data", "name", true, true, "name", true);
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, search, new List<Order>() { order }, new List<DataTableColumn>() { dataColumn });

            var response = await controller.GetAsync(dataTablesRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            Assert.Equal("name", dataColumn.Name);
            Assert.True(dataColumn.Searchable);
            Assert.True(dataColumn.Orderable);
            Assert.Equal("name", dataColumn.SearchValue);
            Assert.True(dataColumn.SearchRegEx);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public void Get_Success_ReturnsViewResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = controller.Get();

            _ = Assert.IsAssignableFrom<ViewResult>(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task Get_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<PagedResult<ApiRoleGetResponseModel>>(null, _errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);
            var dataTablesRequest = CreateDataTablesRequest();

            var result = await controller.GetAsync(dataTablesRequest).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task GetRoleInfo_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<RoleViewModel>(new RoleViewModel { Name = "anyName", Translations = new List<RoleTranslationsModel> { new RoleTranslationsModel { Description = "anyDescription", DisplayText = "anyDisplayText", Language = LanguageType.en } } }, _errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.GetRoleInfoAsync("Admin1").ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task GetRoleInfo_Success_ReturnsViewResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<RoleViewModel>(new RoleViewModel() { Name = "roleName" });
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.GetRoleInfoAsync("Admin").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<PartialViewResult>(result);
            var viewResult = (PartialViewResult)result;
            var resultItem = (RoleViewModel)viewResult.Model;
            Assert.Equal("roleName", resultItem.Name);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task GetRoleInfo_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<RoleViewModel>(null, _errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.GetRoleInfoAsync("Admin").ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task GetRoleUserInfo_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<List<ApiUserRoleInfoResponseModel>>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.GetRoleUserInfoAsync("Admin").ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task GetRoleUserInfo_OkAPIResponse_ReturnsOK()
        {
            var model = new ApiUserRoleInfoResponseModel()
            {
                Email = "test@test.com",
                Id = Guid.NewGuid().ToString(),
                IsInRole = true,
                Username = "anyName",
                Name = "anyName",
                Surname = "anySurname",
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<List<ApiUserRoleInfoResponseModel>>(new List<ApiUserRoleInfoResponseModel>() { model });
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.GetRoleUserInfoAsync("Admin").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task GetRoleUserInfo_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<List<ApiUserRoleInfoResponseModel>>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.GetRoleUserInfoAsync("Admin").ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task Post_NotFoudAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(_errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);
            var roleViewModel = CreateRoleViewModel();

            var result = await controller.PostAsync(roleViewModel).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task Post_OkAPIResponse_ReturnsToastSuccess()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse();

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);
            var roleViewModel = CreateRoleViewModel();

            var result = await controller.PostAsync(roleViewModel).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task Post_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(_errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);
            var roleViewModel = CreateRoleViewModel();

            var result = await controller.PostAsync(roleViewModel).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task Put_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(_errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);
            var roleViewModel = CreateRolePutViewModel();

            var result = await controller.PutAsync(roleViewModel).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task Put_OkAPIResponse_ReturnsToastSuccess()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse();

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);
            var roleViewModel = CreateRolePutViewModel();

            var result = await controller.PutAsync(roleViewModel).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task Put_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(_errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);
            var roleViewModel = CreateRolePutViewModel();

            var result = await controller.PutAsync(roleViewModel).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task RoleClaims_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<PagedResult<RoleViewModel>>(null, _errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.RoleClaimsAsync().ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task RoleClaims_Success_ReturnsViewResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var models = new List<RoleViewModel>() { new RoleViewModel() { Name = "anyName" } };
            var data = new ServiceResponse<PagedResult<RoleViewModel>>(new PagedResult<RoleViewModel>() { Items = models });
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.RoleClaimsAsync().ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<ViewResult>(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task RoleClaims_UnauthorizedResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<PagedResult<RoleViewModel>>(null, _errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.RoleClaimsAsync().ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task RoleClaimswithRolename_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<List<ApiClaimTreeViewItem>>(null, _errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.RoleClaimsAsync("anyName").ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task RoleClaimswithRolename_OkAPIResponse_ReturnsToastError()
        {
            var claimState = new ApiClaimTreeViewItemStateInfo()
            {
                Disabled = false,
                Opened = false,
                Selected = true,
            };
            var claimTree = new ApiClaimTreeViewItem()
            {
                Id = Guid.NewGuid().ToString(),
                Children = new List<ApiClaimTreeViewItem>(),
                State = claimState,
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<List<ApiClaimTreeViewItem>>(new List<ApiClaimTreeViewItem>() { claimTree });

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.RoleClaimsAsync("anyName").ConfigureAwait(false);

            Assert.IsAssignableFrom<OkObjectResult>(result);
            var response = (OkObjectResult)result;
            var resultItem = (List<ApiClaimTreeViewItem>)response.Value;
            Assert.Single(resultItem);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task RoleClaimswithRolename_UnauthorizedResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<List<ApiClaimTreeViewItem>>(null, _errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.RoleClaimsAsync("anyName").ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task SaveRoleClaims_NotFoundResponse_ReturnsToastError()
        {
            var model = new SaveRoleClaimsModel()
            {
                Name = "anyName",
                SelectedRoleClaimList = new List<string>() { "anyClaim" },
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(_errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.SaveRoleClaimsAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task SaveRoleClaims_OkAPIResponse_ReturnsToastSuccess()
        {
            var model = new SaveRoleClaimsModel()
            {
                Name = "anyName",
                SelectedRoleClaimList = new List<string>() { "anyClaim" },
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(null, true);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.SaveRoleClaimsAsync(model).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
            var resultValue = JObject.Parse(JsonConvert.SerializeObject(result.Value));
            Assert.Equal("Success", resultValue["Type"].Value<string>());
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task SaveRoleClaims_UnauthorizedResponse_ReturnsToastError()
        {
            var model = new SaveRoleClaimsModel()
            {
                Name = "anyName",
                SelectedRoleClaimList = new List<string>() { "anyClaim" },
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(_errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.SaveRoleClaimsAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task Search_NotFound_ReturnsToastError()
        {
            var req = new RoleSearchRequest()
            {
                Name = "anyName",
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<RoleController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<PagedResult<ApiRoleGetResponseModel>>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.SearchAsync(req).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task Search_UnaouthorizedAPIResponse_ReturnToastError()
        {
            var req = new RoleSearchRequest()
            {
                Name = "anyName",
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse<PagedResult<ApiRoleGetResponseModel>>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.SearchAsync(req).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "RoleController-MVC")]
        public async Task Search_Success_ReturnOkObjectResult()
        {
            var req = new RoleSearchRequest()
            {
                Name = "anyName",
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<PagedResult<ApiRoleGetResponseModel>>(new PagedResult<ApiRoleGetResponseModel>());
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateRoleController(httpClientFactory, proxyHelper);

            var result = await controller.SearchAsync(req).ConfigureAwait(false);

            Assert.IsAssignableFrom<OkObjectResult>(result);
            var response = (OkObjectResult)result;
            Assert.IsAssignableFrom<JsonDataTableObject<ApiRoleGetResponseModel>>(response.Value);
        }

        private static void AssertToastError(JsonResult toastResult)
        {
            var resultValue = JObject.Parse(JsonConvert.SerializeObject(toastResult.Value));
            Assert.Equal("Error", resultValue["Type"].Value<string>());
            Assert.StartsWith("sometext", resultValue["Message"].Value<string>(), StringComparison.CurrentCulture);
            Assert.Equal("False", resultValue["IsRedirect"].Value<string>());
        }

        private static DataTablesRequest CreateDataTablesRequest()
        {
            return new DataTablesRequest(1, 0, 10, null, null, null);
        }

        private RoleController CreateRoleController(IHttpClientFactory httpClientFactory, IClientProxy proxyHelper)
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());

            var controller = new RoleController(_localizationMock.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext,
                },
                TempData = tempData,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock
                .Setup(serviceProvider => serviceProvider.GetService(typeof(IHttpClientFactory)))
                .Returns(httpClientFactory);

            _ = serviceProviderMock
             .Setup(serviceProvider => serviceProvider.GetService(typeof(IClientProxy)))
             .Returns(proxyHelper);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            return controller;
        }

        private static RolePutViewModel CreateRolePutViewModel()
        {
            return new RolePutViewModel()
            {
                Name = "Admin",
                Translations = new List<RoleTranslationsModel>
                {
                    new RoleTranslationsModel { Description = "anyDescription", DisplayText = "anyDisplayText", Language = LanguageType.en },
                },
            };
        }

        private static RolePostModel CreateRoleViewModel()
        {
            return new RolePostModel()
            {
                Name = "Admin",
                Translations = new List<RoleTranslationsModel>
                {
                    new RoleTranslationsModel { Description = "anyDescription", DisplayText = "anyDisplayText", Language = LanguageType.en },
                },
            };
        }
    }
}