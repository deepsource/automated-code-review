﻿using KocSistem.CommunityEdition.Infrastructure.Helpers.Client;
using KocSistem.CommunityEdition.Mvc.Controllers;
using KocSistem.CommunityEdition.Mvc.Models.DataTables;
using KocSistem.CommunityEdition.Mvc.Models.Language;
using KocSistem.CommunityEdition.Mvc.Models.Paging;
using KocSistem.CommunityEdition.Mvc.Tests.Helpers;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Localization;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Mvc.Tests.Controllers
{
    public class LanguageControllerTest
    {
        private readonly Mock<IHttpClientFactory> _httpClientFactoryMock;
        private readonly Mock<IClientProxy> _proxyHelperMock;
        private readonly Mock<IKsI18N> _localizationMoq;
        private readonly ErrorInfo _errorInfo = new("sometext");
        private readonly string _sampleResponse = "{\"pageIndex\":0,\"pageSize\":1,\"totalCount\":2,\"totalPages\":1,\"items\":[{\"id\":\"2d27aa6e-ba08-4468-8614-280351e7d642\",\"name\":\"Admin\",\"description\":\"Admin desc\"}]}";

        public LanguageControllerTest()
        {
            _httpClientFactoryMock = new Mock<IHttpClientFactory>();
            _proxyHelperMock = new Mock<IClientProxy>();
            _localizationMoq = new Mock<IKsI18N>();
            _ = _localizationMoq.SetupGet(x => x.GetLocalizer<LanguageController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
        }


        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public void Get_ReturnsViewResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<List<LanguageViewModel>>(null);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = controller.Get() as ViewResult;

            _ = Assert.IsAssignableFrom<ViewResult>(result);
        }

        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task Post_NotFound_ReturnToastError()
        {
            var model = new LanguagePostModel()
            {   
                Name = "English",
                Code = "en-En",
                Image = "",
                IsActive = true,
                IsDefault = true,             
            };

            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = await controller.PostAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task Post_UnauthorizedAPIResponse_ReturnToastError()
        {
            var model = new LanguagePostModel()
            {
                Name = "English",
                Code = "en-En",
                Image = "",
                IsActive = true,
                IsDefault = true,
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = await controller.PostAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task GetLanguageInfo_NotFound_ReturnToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse<LanguageViewModel>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = await controller.GetLanguageInfoAsync(Guid.NewGuid()).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task GetLanguageInfo_UnauthorizedAPIResponse_ReturnToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse<LanguageViewModel>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = await controller.GetLanguageInfoAsync(Guid.NewGuid()).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task GetLanguageInfo_Success_ReturnViewResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);

            var data = new ServiceResponse<LanguageViewModel>(new LanguageViewModel());
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = await controller.GetLanguageInfoAsync(Guid.NewGuid()).ConfigureAwait(false);

            Assert.IsAssignableFrom<PartialViewResult>(result);
            var response = (PartialViewResult)result;
            Assert.IsAssignableFrom<LanguageViewModel>(response.Model);
            var model = (LanguageViewModel)response.Model;
            Assert.Equal(data.Result.Id, model.Id);
        }

        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task GetList_RequestModelSearchNotNull_ReturnOkObjectResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<PagedResult<LanguageGetResponseModel>>(new PagedResult<LanguageGetResponseModel>());
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);
            var search = new Search("anyString", true);
            var order = new Order(0, "anyString");
            var dataColumn = new DataTableColumn("data", "name", true, true, "name", true);
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, search, new List<Order>() { order }, new List<DataTableColumn>() { dataColumn });

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
            Assert.Equal("name", dataColumn.Name);
            Assert.True(dataColumn.Searchable);
            Assert.True(dataColumn.Orderable);
            Assert.Equal("name", dataColumn.SearchValue);
            Assert.True(dataColumn.SearchRegEx);
        }

        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task GetList_Success_ReturnOkObjectResult()
        {
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, null, null, null);
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<PagedResult<LanguageGetResponseModel>>(new PagedResult<LanguageGetResponseModel>());
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task GetList_NotFound_ReturnToastError()
        {
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, null, null, null);

            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse<PagedResult<LanguageGetResponseModel>>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task GetList_UnauthorizedAPIResponse_ReturnToastError()
        {
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, null, null, null);
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse<PagedResult<LanguageGetResponseModel>>(new PagedResult<LanguageGetResponseModel>(), _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task Post_OkAPIResponse_ReturnsToastSuccessForRedirect()
        {
            var model = new LanguagePostModel()
            {
                Name = "English",
                Code = "en-En",
                Image = "",
                IsActive = true,
                IsDefault = true,
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = await controller.PostAsync(model).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task Put_NotFound_ReturnToastError()
        {
            var model = new LanguagePutModel()
            {
                Id = new Guid("5a41be5e-0cb9-4a3e-a1a7-0244b53134cc"),
                Name = "English",
                Code = "en-En",
                Image = "",
                IsActive = true,
                IsDefault = true,
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = await controller.PutAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task Put_UnauthorizedAPIResponse_ReturnToastError()
        {
            var model = new LanguagePutModel()
            {
                Id = new Guid("5a41be5e-0cb9-4a3e-a1a7-0244b53134cc"),
                Name = "English",
                Code = "en-En",
                Image = "",
                IsActive = true,
                IsDefault = true,
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = await controller.PutAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task Put_OkAPIResponse_ReturnsToastSuccessForRedirect()
        {
            var model = new LanguagePutModel()
            {
                Id = new Guid("5a41be5e-0cb9-4a3e-a1a7-0244b53134cc"),
                Name = "English",
                Code = "en-En",
                Image = "",
                IsActive = true,
                IsDefault = true,
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = await controller.PutAsync(model).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task Delete_NotFound_ReturnToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = await controller.DeleteAsync(Guid.NewGuid()).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task Delete_UnauthorizedAPIResponse_ReturnToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = await controller.DeleteAsync(Guid.NewGuid()).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task Delete_OkAPIResponse_ReturnsToastSuccessForRedirect()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = await controller.DeleteAsync(Guid.NewGuid()).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }


        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task Search_NotFound_ReturnToastError()
        {
            var model = new LanguageSearchRequest() { Key = "anykey", Orders = new List<PagedRequestOrder>(), PageIndex = 1, PageSize = 2 };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse<PagedResult<LanguageGetResponseModel>>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = await controller.SearchAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task Search_UnauthorizedAPIResponse_ReturnToastError()
        {
            var model = new LanguageSearchRequest() { Key = "anykey", Orders = new List<PagedRequestOrder>(), PageIndex = 1, PageSize = 2 };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse<PagedResult<LanguageGetResponseModel>>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = await controller.SearchAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LanguageController-MVC")]
        public async Task Search_Success_ReturnOkObjectResult()
        {
            var model = new LanguageSearchRequest() { Key = "anykey", Orders = new List<PagedRequestOrder>(), PageIndex = 1, PageSize = 2 };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<PagedResult<LanguageGetResponseModel>>(new PagedResult<LanguageGetResponseModel>());
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLanguageController(httpClientFactory, proxyHelper);

            var result = await controller.SearchAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        private static void AssertToastError(JsonResult toastResult)
        {
            var resultValue = JObject.Parse(JsonConvert.SerializeObject(toastResult.Value));
            Assert.Equal("Error", resultValue["Type"].Value<string>());
            Assert.StartsWith("sometext", resultValue["Message"].Value<string>(), StringComparison.CurrentCulture);
            Assert.Equal("False", resultValue["IsRedirect"].Value<string>());
        }

        private LanguageController CreateLanguageController(IHttpClientFactory httpClientFactory, IClientProxy proxyHelper, IUrlHelperFactory urlHelperFactory = null, List<Claim> claims = null)
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());

            var controller = new LanguageController(_localizationMoq.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext,
                },
                TempData = tempData,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock
                .Setup(serviceProvider => serviceProvider.GetService(typeof(IHttpClientFactory)))
                .Returns(httpClientFactory);

            _ = serviceProviderMock
             .Setup(serviceProvider => serviceProvider.GetService(typeof(IClientProxy)))
             .Returns(proxyHelper);

            if (urlHelperFactory != null)
            {
                _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IUrlHelperFactory))).Returns(urlHelperFactory);
            }

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            if (claims != null)
            {
                var identity = new ClaimsIdentity(claims: claims, authenticationType: "Test");
                var claimsPrincipal = new ClaimsPrincipal(identity: identity);
                var mockPrincipal = new Mock<IPrincipal>();
                _ = mockPrincipal.Setup(expression: x => x.Identity).Returns(value: identity);
                _ = mockPrincipal.Setup(expression: x => x.IsInRole(It.IsAny<string>())).Returns(value: true);

                controller.ControllerContext.HttpContext.User = claimsPrincipal;
            }

            return controller;
        }
    }
}
