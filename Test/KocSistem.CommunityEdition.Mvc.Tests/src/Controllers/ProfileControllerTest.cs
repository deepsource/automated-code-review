﻿// <copyright file="ProfileControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Application.Abstractions.ApplicationSetting;
using KocSistem.CommunityEdition.Infrastructure.Helpers.Client;
using KocSistem.CommunityEdition.Mvc.Controllers;
using KocSistem.CommunityEdition.Mvc.Models.Profile;
using KocSistem.CommunityEdition.Mvc.Models.User;
using KocSistem.CommunityEdition.Mvc.Tests.Helpers;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Mvc.Tests.Controllers
{
    public class ProfileControllerTest
    {
        private readonly Mock<IKsI18N> _localizationMoq;
        private readonly Mock<IClientProxy> _proxyHelperMock;
        private readonly Mock<IHttpClientFactory> _httpClientFactoryMock;
        private readonly ErrorInfo _errorInfo = new("sometext");
        private readonly string _sampleResponse = "{\"pageIndex\":0,\"pageSize\":1,\"totalCount\":2,\"totalPages\":1,\"items\":[{\"id\":\"2d27aa6e-ba08-4468-8614-280351e7d642\",\"name\":\"Admin\",\"description\":\"Admin desc\"}]}";
        private readonly Mock<IUrlHelperFactory> _urlHelperFactoryMock;
        private readonly Mock<IUrlHelper> _urlHelperMock;
        private readonly Mock<IConfiguration> _configurationMoq;

        public ProfileControllerTest()
        {
            _localizationMoq = new Mock<IKsI18N>();
            _ = _localizationMoq.SetupGet(x => x.GetLocalizer<ProfileController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _proxyHelperMock = new Mock<IClientProxy>();
            _httpClientFactoryMock = new Mock<IHttpClientFactory>();
            _urlHelperFactoryMock = new Mock<IUrlHelperFactory>();
            _urlHelperMock = new Mock<IUrlHelper>();
            _configurationMoq = new Mock<IConfiguration>();
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public void ChangePassword_Success_ReturnsViewResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse<ProfileModel>(null, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateProfileController(httpClientFactory, proxyHelper);

            var result = controller.ChangePassword();

            _ = Assert.IsAssignableFrom<PartialViewResult>(result);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task Index_NotFound_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse<ProfileModel>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateProfileController(httpClientFactory, proxyHelper);

            var result = await controller.IndexAsync().ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task Index_Success_ReturnsViewResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<ProfileModel>(new ProfileModel() { Name = "John" });
            var applicationSettingsResponse = new ServiceResponse<ApplicationSettingDto>(new ApplicationSettingDto { Key = "Identity:ProfilePhoto:MaxSize", Value = "160000" });
            var timeZoneResponse = new ServiceResponse<List<TimeZoneModel>>(new List<TimeZoneModel>() { new TimeZoneModel { Id = "Europe/Istanbul", DisplayName = "(UTC+03:00) Istanbul" }, new TimeZoneModel { Id = "Etc/GMT+12", DisplayName = "(UTC-12:00) International Date Line West" } });

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            proxyHelper = _proxyHelperMock.GetProxyHelper(applicationSettingsResponse);
            proxyHelper = _proxyHelperMock.GetProxyHelper(timeZoneResponse);

            using var controller = CreateProfileController(httpClientFactory, proxyHelper);

            var result = await controller.IndexAsync().ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<ViewResult>(result);
            var viewResult = (ViewResult)result;
            var resultItem = (ProfileModel)viewResult.Model;
            Assert.Equal("John", resultItem.Name);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task SetChangePassword_NotFound_ReturnsToastError()
        {
            var req = new ChangePasswordModel()
            {
                CurrentPassword = "123456",
                NewPasswordConfirmation = "654321",
                NewPassword = "654321",
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateProfileController(httpClientFactory, proxyHelper);

            var result = await controller.SetChangePasswordAsync(req).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task SetChangePassword_NewPasswordSameWithCurrentOne_ReturnsToastError()
        {
            var req = new ChangePasswordModel()
            {
                CurrentPassword = "123456",
                NewPasswordConfirmation = "123456",
                NewPassword = "123456",
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateProfileController(httpClientFactory, proxyHelper);

            var result = await controller.SetChangePasswordAsync(req).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task SetChangePassword_Success_ToastSuccessForRedirect()
        {
            var req = new ChangePasswordModel()
            {
                CurrentPassword = "123456",
                NewPasswordConfirmation = "654321",
                NewPassword = "654321",
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateProfileController(httpClientFactory, proxyHelper);

            var result = await controller.SetChangePasswordAsync(req).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task SetProfileInfo_NotFound_ReturnsToastError()
        {
            var req = new ProfileModel()
            {
                Email = "test@test.com",
                EmailConfirmed = true,
                Name = "John",
                Surname = "Doe",
                PhoneNumber = "00000000000",
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateProfileController(httpClientFactory, proxyHelper);

            var result = await controller.SetProfileInfoAsync(req).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task SetProfileInfo_Success_ReturnsToastSuccessForRedirect()
        {
            var req = new ProfileModel()
            {
                Email = "test@test.com",
                EmailConfirmed = true,
                Name = "John",
                Surname = "Doe",
                PhoneNumber = "00000000000",
                TimeZone= "Europe/Istanbul"
            };

            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateProfileController(httpClientFactory, proxyHelper);

            var result = await controller.SetProfileInfoAsync(req).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task SetProfilePhoto_NotFound_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateProfileController(httpClientFactory, proxyHelper);

            var result = await controller.SetProfilePhotoAsync(It.IsAny<string>()).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task SetProfilePhoto_Success_ReturnsToastSuccessForRedirect()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateProfileController(httpClientFactory, proxyHelper);

            var result = await controller.SetProfilePhotoAsync(It.IsAny<string>()).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task GetProfilePhoto_NotFound_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse<ProfileModel>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateProfileController(httpClientFactory, proxyHelper);

            var result = await controller.GetProfilePhotoAsync().ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task GetProfilePhoto_Success_ReturnsToastSuccessForRedirect()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<ProfileModel>(new ProfileModel());
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateProfileController(httpClientFactory, proxyHelper);

            var result = await controller.GetProfilePhotoAsync().ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task DeleteProfilePhoto_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateProfileController(httpClientFactory, proxyHelper);

            var result = await controller.DeleteProfilePhotoAsync().ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task DeleteProfilePhoto_Success()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateProfileController(httpClientFactory, proxyHelper);

            var result = await controller.DeleteProfilePhotoAsync().ConfigureAwait(true);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task GetConfirmationInfos_NoContent_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NoContent, _sampleResponse);
            var data = new ServiceResponse<ConfirmationCodeModel>(null, new ErrorInfo { Code = StatusCodes.Status400BadRequest, Message = "sometext" }, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateProfileController(httpClientFactory, proxyHelper);

            var result = await controller.GetConfirmationInfosAsync("00000000000").ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task GetConfirmationInfos_BadRequest_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.BadRequest, _sampleResponse);
            var data = new ServiceResponse<ConfirmationCodeModel>(null, new ErrorInfo { Code = StatusCodes.Status400BadRequest, Message = "sometext" }, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateProfileController(httpClientFactory, proxyHelper);

            var result = await controller.GetConfirmationInfosAsync("00000000000").ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task GetConfirmationInfos_NotFound_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse<ConfirmationCodeModel>(null, new ErrorInfo { Code = StatusCodes.Status400BadRequest, Message = "sometext" }, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateProfileController(httpClientFactory, proxyHelper);

            var result = await controller.GetConfirmationInfosAsync("00000000000").ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task GetConfirmationInfos_Success_ReturnsViewResult()
        {
            var expiredDate = DateTime.UtcNow.AddMinutes(2);
            var responseData = new ConfirmationCodeModel()
            {
                Id = Guid.Parse("9c15bad3-56d6-4c8e-9b03-5d3201194737"),
                PhoneNumber = "00000000000",
                ExpiredDate = expiredDate,
                Code = "123456",
                IsSent = true
            };

            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<ConfirmationCodeModel>(responseData);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateProfileController(httpClientFactory, proxyHelper);

            var result = await controller.GetConfirmationInfosAsync("00000000000").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<ViewResult>(result);
            var viewResult = (ViewResult)result;
            var resultItem = (ConfirmationCodeViewModel)viewResult.Model;

            Assert.Equal(Guid.Parse("9c15bad3-56d6-4c8e-9b03-5d3201194737"), resultItem.Id);
            Assert.Equal("00000000000", resultItem.PhoneNumber);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task ConfirmPhoneNumber_NoContent_ReturnsToastError()
        {
            var req = new ConfirmationCodeViewModel()
            {
                Id = Guid.Parse("9c15bad3-56d6-4c8e-9b03-5d3201194737"),
                Code = "000000",
                PhoneNumber = "00000000000"
            };

            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NoContent, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            var urlHelperFactory = CreateUrlHelperFactory();
            using var controller = CreateProfileController(httpClientFactory, proxyHelper, urlHelperFactory);

            var result = await controller.ConfirmPhoneNumberAsync(req).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task ConfirmPhoneNumber_BadRequest_ReturnsToastError()
        {
            var req = new ConfirmationCodeViewModel()
            {
                Id = Guid.Parse("9c15bad3-56d6-4c8e-9b03-5d3201194737"),
                Code = "000000",
                PhoneNumber = "00000000000"
            };

            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.BadRequest, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            var urlHelperFactory = CreateUrlHelperFactory();
            using var controller = CreateProfileController(httpClientFactory, proxyHelper, urlHelperFactory);

            var result = await controller.ConfirmPhoneNumberAsync(req).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ProfileController-MVC")]
        public async Task ConfirmPhoneNumber_Success_ReturnsToastSuccessForRedirect()
        {
            var req = new ConfirmationCodeViewModel()
            {
                Id = Guid.Parse("9c15bad3-56d6-4c8e-9b03-5d3201194737"),
                Code = "000000",
                PhoneNumber = "00000000000"
            };

            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            var urlHelperFactory = CreateUrlHelperFactory();
            using var controller = CreateProfileController(httpClientFactory, proxyHelper, urlHelperFactory);

            var result = await controller.ConfirmPhoneNumberAsync(req).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        private static void AssertToastError(JsonResult toastResult)
        {
            var resultValue = JObject.Parse(JsonConvert.SerializeObject(toastResult.Value));
            Assert.Equal("Error", resultValue["Type"].Value<string>());
            Assert.StartsWith("sometext", resultValue["Message"].Value<string>(), StringComparison.CurrentCulture);
            Assert.Equal("False", resultValue["IsRedirect"].Value<string>());
        }

        private IUrlHelperFactory CreateUrlHelperFactory()
        {
            _ = _urlHelperMock.Setup(s => s.Action(It.IsAny<UrlActionContext>())).Returns("anyString");
            _ = _urlHelperFactoryMock.Setup(s => s.GetUrlHelper(It.IsAny<ActionContext>())).Returns(_urlHelperMock.Object);
            return _urlHelperFactoryMock.Object;
        }

        private ProfileController CreateProfileController(IHttpClientFactory httpClientFactory, IClientProxy proxyHelper, IUrlHelperFactory urlHelperFactory = null, List<Claim> claims = null)
        {
            var httpContext = new DefaultHttpContext();

            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());

            var controller = new ProfileController(
                i18N: _localizationMoq.Object,
                configuration: _configurationMoq.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext,
                },
                TempData = tempData,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock
                .Setup(serviceProvider => serviceProvider.GetService(typeof(IHttpClientFactory)))
                .Returns(httpClientFactory);

            _ = serviceProviderMock
             .Setup(serviceProvider => serviceProvider.GetService(typeof(IClientProxy)))
             .Returns(proxyHelper);

            if (urlHelperFactory != null)
            {
                _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IUrlHelperFactory))).Returns(urlHelperFactory);
            }

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            return controller;
        }
    }
}