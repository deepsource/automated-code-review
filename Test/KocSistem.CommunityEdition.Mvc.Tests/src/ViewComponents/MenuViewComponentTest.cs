﻿// <copyright file="MenuViewComponentTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Infrastructure.Helpers.Client;
using KocSistem.CommunityEdition.Mvc.Models.Menu;
using KocSistem.CommunityEdition.Mvc.Tests.Helpers;
using KocSistem.CommunityEdition.Mvc.ViewComponents.Menu;
using KocSistem.OneFrame.DesignObjects.Services;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Moq;
using System.Collections.Generic;
using Xunit;

namespace KocSistem.CommunityEdition.Mvc.Tests.ViewComponents
{
    public class MenuViewComponentTest
    {
        private readonly Mock<IClientProxy> _proxyHelperMock;

        public MenuViewComponentTest()
        {
            _proxyHelperMock = new Mock<IClientProxy>();
        }

        [Fact]
        [Trait("Category", "Components")]
        public void InvokeAsync_WithNoMenuModels_ReturnsViewViewComponentResult()
        {
            var data = new ServiceResponse<IEnumerable<MenuModel>>(new List<MenuModel>());
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            var menuViewComponent = new MenuViewComponent(proxyHelper);

            var result = menuViewComponent.InvokeAsync();

            _ = Assert.IsAssignableFrom<ViewViewComponentResult>(result.Result);
        }

        [Fact]
        [Trait("Category", "Components")]
        public void InvokeAsync_WithMenuModels_ReturnsViewViewComponentResult()
        {
            var data = new ServiceResponse<IEnumerable<MenuModel>>(new List<MenuModel>() { new MenuModel() });
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            var menuViewComponent = new MenuViewComponent(proxyHelper);

            var result = menuViewComponent.InvokeAsync();

            _ = Assert.IsAssignableFrom<ViewViewComponentResult>(result.Result);
        }
    }
}
