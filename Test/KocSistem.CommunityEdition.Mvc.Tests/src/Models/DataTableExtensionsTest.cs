﻿// <copyright file="DataTableExtensionsTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Domain;
using KocSistem.CommunityEdition.Mvc.Models.DataTables;
using System.Collections.Generic;
using Xunit;

namespace KocSistem.CommunityEdition.Mvc.Tests.Models
{
    public class DataTableExtensionsTest
    {
        [Fact]
        [Trait("Category", "Binder-MVC")]
        public void DataTablesExtensions_ToDataTablesResponse_Success()
        {
            var search = new Search("anyString", true);
            var order = new Order(0, "anyString");
            var dataColumn = new DataTableColumn("data", "name", true, true, "name", true);
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, search, new List<Order>() { order }, new List<DataTableColumn>() { dataColumn });
            var list = new List<ApplicationUser>() { new ApplicationUser() { UserName = "test@test.com", Email = "test@test.com" }, new ApplicationUser() { UserName = "test2@test.com", Email = "test2@test.com" } };

            var result = list.ToDataTablesResponse(dataTablesRequest);

            Assert.Null(result.Error);
            Assert.Equal(1, result.Draw);
            Assert.Equal(2, result.RecordsFiltered);
            Assert.Equal(2, result.RecordsTotal);
        }

        [Fact]
        [Trait("Category", "Binder-MVC")]
        public void DataTablesExtensions_ToDataTablesResponsewithRecordsTotalParameter_Success()
        {
            var search = new Search("anyString", true);
            var order = new Order(0, "anyString");
            var dataColumn = new DataTableColumn("data", "name", true, true, "name", true);
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, search, new List<Order>() { order }, new List<DataTableColumn>() { dataColumn });
            var list = new List<ApplicationUser>() { new ApplicationUser() { UserName = "test@test.com", Email = "test@test.com" }, new ApplicationUser() { UserName = "test2@test.com", Email = "test2@test.com" } };

            var result = list.ToDataTablesResponse(dataTablesRequest, 2);

            Assert.Null(result.Error);
            Assert.Equal(1, result.Draw);
            Assert.Equal(2, result.RecordsFiltered);
            Assert.Equal(2, result.RecordsTotal);
        }

        [Fact]
        [Trait("Category", "Binder-MVC")]
        public void DataTablesExtensions_ToDataTablesResponsewithRecordsTotalandRecordsFilteredParameter_Success()
        {
            var search = new Search("anyString", true);
            var order = new Order(0, "anyString");
            var dataColumn = new DataTableColumn("data", "name", true, true, "name", true);
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, search, new List<Order>() { order }, new List<DataTableColumn>() { dataColumn });
            var list = new List<ApplicationUser>() { new ApplicationUser() { UserName = "test@test.com", Email = "test@test.com" }, new ApplicationUser() { UserName = "test2@test.com", Email = "test2@test.com" } };

            var result = list.ToDataTablesResponse(dataTablesRequest, 2, 2);

            Assert.Null(result.Error);
            Assert.Equal(1, result.Draw);
            Assert.Equal(2, result.RecordsFiltered);
            Assert.Equal(2, result.RecordsTotal);
        }
    }
}
