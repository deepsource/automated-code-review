﻿// <copyright file="GoogleReCaptchaValidatorTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Infrastructure.Helpers.Captcha;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Moq;
using Moq.Protected;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Infrastructure.Tests.Helpers
{
    public class GoogleReCaptchaValidatorTest
    {
        private readonly Mock<IConfiguration> _configurationMoq;
        private readonly Mock<IHttpClientFactory> _httpClientFactoryMoq;
        private readonly Mock<IHttpContextAccessor> _httpContextAccessorMoq;
        private readonly DefaultHttpContext _context;
        private readonly Mock<IServiceProvider> _serviceProviderMoq;

        public GoogleReCaptchaValidatorTest()
        {
            _configurationMoq = new Mock<IConfiguration>();
            _context = new DefaultHttpContext();
            _httpClientFactoryMoq = new Mock<IHttpClientFactory>();
            _httpContextAccessorMoq = new Mock<IHttpContextAccessor>();
            _serviceProviderMoq = new Mock<IServiceProvider>();

            _ = _httpContextAccessorMoq.Setup(r => r.HttpContext).Returns(_context);
            _ = _serviceProviderMoq.Setup(s => s.GetService(It.IsAny<Type>())).Returns(_httpClientFactoryMoq.Object);
            _ = _httpContextAccessorMoq.Setup(r => r.HttpContext.RequestServices).Returns(_serviceProviderMoq.Object);

            _ = _configurationMoq.SetupGet(x => x[It.Is<string>(s => s == "ReCaptcha:SecretKey")]).Returns("10");
            _ = _configurationMoq.SetupGet(x => x[It.Is<string>(s => s == "ReCaptcha:RemoteAddress")]).Returns("https://www.google.com/recaptcha/api.js");
        }

        [Fact]
        public async Task IsCaptchaPassedAsync_HttpStatusCodeOK_ReturnsFalse()
        {
            _ = _httpClientFactoryMoq.Setup(r => r.CreateClient(It.IsAny<string>())).Returns(new HttpClient());
            var googleReCaptchaValidator = new GoogleReCaptchaValidator(_httpContextAccessorMoq.Object, _configurationMoq.Object);

            var result = await googleReCaptchaValidator.IsCaptchaPassedAsync("token").ConfigureAwait(false);
            Assert.False(result);
        }

        [Fact]
        public async Task IsCaptchaPassedAsync_NotSuccess_ReturnsFalse()
        {
            var mockHttpMessageHandler = new Mock<HttpMessageHandler>();
            _ = mockHttpMessageHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage { Content = new StringContent(@"{'success' : ''}"), StatusCode = HttpStatusCode.OK });
            var httpClient = new HttpClient(mockHttpMessageHandler.Object);
            _ = _httpClientFactoryMoq.Setup(r => r.CreateClient(It.IsAny<string>())).Returns(httpClient);
            var googleReCaptchaValidator = new GoogleReCaptchaValidator(_httpContextAccessorMoq.Object, _configurationMoq.Object);

            var result = await googleReCaptchaValidator.IsCaptchaPassedAsync("token").ConfigureAwait(false);
            Assert.False(result);
        }

        [Fact]
        public async Task IsCaptchaPassedAsync_Success_ReturnsTrue()
        {
            var mockHttpMessageHandler = new Mock<HttpMessageHandler>();
            _ = mockHttpMessageHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage { Content = new StringContent(@"{'success' : 'true'}"), StatusCode = HttpStatusCode.OK });
            var httpClient = new HttpClient(mockHttpMessageHandler.Object);
            _ = _httpClientFactoryMoq.Setup(r => r.CreateClient(It.IsAny<string>())).Returns(httpClient);
            var googleReCaptchaValidator = new GoogleReCaptchaValidator(_httpContextAccessorMoq.Object, _configurationMoq.Object);

            var result = await googleReCaptchaValidator.IsCaptchaPassedAsync("token").ConfigureAwait(false);
            Assert.True(result);
        }

        [Fact]
        public void UpdateSecretKey_Successful()
        {
            var key = "testKey";
            var googleReCaptchaValidator = new GoogleReCaptchaValidator(_httpContextAccessorMoq.Object, _configurationMoq.Object);

            googleReCaptchaValidator.UpdateSecretKey(key);
            Assert.True(true);
        }
    }
}
