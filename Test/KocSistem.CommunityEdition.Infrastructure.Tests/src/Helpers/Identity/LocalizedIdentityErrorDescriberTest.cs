﻿// <copyright file="LocalizedIdentityErrorDescriberTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Infrastructure.Helpers.Identity;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
using Moq;
using Xunit;

namespace KocSistem.CommunityEdition.Infrastructure.Tests.Helpers.Identity
{
    public class LocalizedIdentityErrorDescriberTest
    {
        private readonly Mock<IKsStringLocalizer<LocalizedIdentityErrorDescriber>> _localizationMoq;

        public LocalizedIdentityErrorDescriberTest()
        {
            _localizationMoq = new Mock<IKsStringLocalizer<LocalizedIdentityErrorDescriber>>();

            _ = _localizationMoq.SetupGet(s => s[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = _localizationMoq.SetupGet(s => s[It.IsAny<string>(), It.IsAny<object[]>()]).Returns(new LocalizedString("sometext", "sometext"));
        }

        [Fact]
        public void ConcurrencyFailure_ReturnsIdentityError()
        {
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.ConcurrencyFailure();
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void DefaultError_ReturnsIdentityError()
        {
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.DefaultError();
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void DuplicateEmail_ReturnsIdentityError()
        {
            var email = "testEmail";
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.DuplicateEmail(email);
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void DuplicateRoleName_ReturnsIdentityError()
        {
            var role = "testRole";
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.DuplicateRoleName(role);
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void DuplicateUserName_ReturnsIdentityError()
        {
            var username = "testUserName";
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.DuplicateUserName(username);
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void InvalidEmail_ReturnsIdentityError()
        {
            var email = "testEmail";
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.InvalidEmail(email);
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void InvalidRoleName_ReturnsIdentityError()
        {
            var role = "testRole";
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.InvalidRoleName(role);
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void InvalidToken_ReturnsIdentityError()
        {
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.InvalidToken();
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void InvalidUserName_ReturnsIdentityError()
        {
            var username = "testUserName";
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.InvalidUserName(username);
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void LoginAlreadyAssociated_ReturnsIdentityError()
        {
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.LoginAlreadyAssociated();
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void PasswordMismatch_ReturnsIdentityError()
        {
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.PasswordMismatch();
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void PasswordRequiresDigit_ReturnsIdentityError()
        {
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.PasswordRequiresDigit();
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void PasswordRequiresLower_ReturnsIdentityError()
        {
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.PasswordRequiresLower();
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void PasswordRequiresNonAlphanumeric_ReturnsIdentityError()
        {
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.PasswordRequiresNonAlphanumeric();
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void PasswordRequiresUniqueChars_ReturnsIdentityError()
        {
            var uniqueChars = 1;
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.PasswordRequiresUniqueChars(uniqueChars);
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void PasswordRequiresUpper_ReturnsIdentityError()
        {
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.PasswordRequiresUpper();
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void PasswordTooShort_ReturnsIdentityError()
        {
            var length = 1;
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.PasswordTooShort(length);
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void RecoveryCodeRedemptionFailed_ReturnsIdentityError()
        {
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.RecoveryCodeRedemptionFailed();
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void UserAlreadyHasPassword_ReturnsIdentityError()
        {
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.UserAlreadyHasPassword();
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void UserAlreadyInRole_ReturnsIdentityError()
        {
            var role = "testRole";
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.UserAlreadyInRole(role);
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void UserLockoutNotEnabled_ReturnsIdentityError()
        {
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.UserLockoutNotEnabled();
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }

        [Fact]
        public void UserNotInRole_ReturnsIdentityError()
        {
            var role = "testRole";
            var localizedIdentityErrorDescriber = new LocalizedIdentityErrorDescriber(_localizationMoq.Object);

            var result = localizedIdentityErrorDescriber.UserNotInRole(role);
            _ = Assert.IsAssignableFrom<IdentityError>(result);
        }
    }
}
