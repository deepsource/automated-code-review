﻿// <copyright file="ServiceCollectionExtensionsTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>


using Microsoft.Extensions.DependencyInjection;
using Moq;
using KocSistem.CommunityEdition.Infrastructure.Extensions;
using Xunit;
using System;
using Microsoft.Extensions.Configuration;

namespace KocSistem.CommunityEdition.Infrastructure.Tests.Extensions
{
    public class ServiceCollectionExtensionsTest
    {
        private readonly Mock<IConfiguration> _configurationMoq;

        public ServiceCollectionExtensionsTest()
        {
            _configurationMoq = new Mock<IConfiguration>();
        }

        [Fact]
        [Trait("Category", "Extensions")]
        public void AddAuthorizationServices_Successful()
        {
            var services = new ServiceCollection();
            var ex = Record.Exception(() => services.AddAuthorizationServices());
            Assert.Null(ex);
        }

        [Fact]
        [Trait("Category", "Extensions")]
        public void AddCorsServices_Successful()
        {
            var services = new ServiceCollection();
            var ex = Record.Exception(() => services.AddCorsServices());
            Assert.Null(ex);
        }

        [Fact]
        [Trait("Category", "Extensions")]
        public void AddDistributedCacheServices_Successful()
        {
            var services = new ServiceCollection();
            var ex = Record.Exception(() => services.AddDistributedCacheServices(_configurationMoq.Object));
            Assert.Null(ex);
        }

        [Fact]
        [Trait("Category", "Extensions")]
        public void AddDistributedCacheServices_ServicesNull_Throws()
        {
            ServiceCollection services = null;
            _ = Assert.Throws<ArgumentNullException>(() => services.AddDistributedCacheServices(_configurationMoq.Object));
        }

        [Fact]
        [Trait("Category", "Extensions")]
        public void AddEmailNotificationsServices_Successful()
        {
            var services = new ServiceCollection();
            var ex = Record.Exception(() => services.AddEmailNotificationsServices(_configurationMoq.Object));
            Assert.Null(ex);
        }

        [Fact]
        [Trait("Category", "Extensions")]
        public void AddIdentityServices_Successful()
        {
            var services = new ServiceCollection();
            var ex = Record.Exception(() => services.AddIdentityServices(_configurationMoq.Object));
            Assert.Null(ex);
        }

        [Fact]
        [Trait("Category", "Extensions")]
        public void AddKsI18NServices_Successful()
        {
            var resourcePath = "resourcePath";
            var services = new ServiceCollection();
            var ex = Record.Exception(() => services.AddKsI18NServices(resourcePath, true));
            Assert.Null(ex);
        }
    }
}
