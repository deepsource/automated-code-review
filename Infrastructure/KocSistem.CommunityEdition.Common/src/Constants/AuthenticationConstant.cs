﻿// <copyright file="AuthenticationConstant.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>
namespace KocSistem.CommunityEdition.Common.Constants
{
    public static class AuthenticationConstant
    {
        public const string KsLoginProvider = "[AspNetUserStore]";
        public const string KsAuthenticatorProvider = "AuthenticatorKey";
    }
}
