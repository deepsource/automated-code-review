﻿// <copyright file="ConfigurationConstant.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>
namespace KocSistem.CommunityEdition.Common.Constants
{
    public static class ConfigurationConstant
    {
        public const string Identity2FaSettingsAuthenticatorLinkName = "Identity:2FASettings:AuthenticatorLinkName";
        public const string Identity2FaSettingsIsEnabled = "Identity:2FASettings:IsEnabled";
        public const string Identity2FaSettingsType = "Identity:2FASettings:Type";
        public const string Identity2FaSettingsVerificationTime = "Identity:2FASettings:VerificationTime";
        public const string IdentityAutoLogoutDialogTimeout = "Identity:AutoLogout:DialogTimeout";
        public const string IdentityAutoLogoutIdleTimeout = "Identity:AutoLogout:IdleTimeout";
        public const string IdentityProfilePhotoMaxSize = "Identity:ProfilePhoto:MaxSize";
        public const string IdentityAutoLogoutIsEnabled = "Identity:AutoLogout:IsEnabled";
        public const string NotificationEmailIsActive = "Notification:Email:IsActive";
        public const string DefaultTimeZone = "Europe/Istanbul";
    }
}