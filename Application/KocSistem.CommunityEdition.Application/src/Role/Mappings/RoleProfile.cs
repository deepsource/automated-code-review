﻿// <copyright file="RoleProfile.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Role.Contracts;
using KocSistem.CommunityEdition.Domain;
using KocSistem.OneFrame.Data.Relational;

namespace KocSistem.CommunityEdition.Application.Role.Mappings
{
    /// <summary>
    ///  Definition User Entity AutoMapper Profiles.
    /// </summary>
    /// <seealso cref="Profile" />
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class RoleProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RoleProfile"/> class.
        /// </summary>
        public RoleProfile()
        {
            _ = CreateMap<ApplicationRole, ApplicationRoleDto>().ReverseMap();
            _ = CreateMap<IPagedList<ApplicationRole>, PagedResultDto<ApplicationRoleDto>>().ReverseMap();
        }
    }
}
