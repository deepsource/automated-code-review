﻿// <copyright file="UserConfirmationHistoryDto.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.OneFrame.Data.Relational;
using System;

namespace KocSistem.CommunityEdition.Application.Abstractions.UserConfirmationCodeHistory.Contracts
{
    /// <summary>
    /// User Confirmation Code History Dto.
    /// </summary>
    /// <seealso cref="Guid" />
    public class UserConfirmationHistoryDto : IDto<Guid>
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public int CodeType { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string Code { get; set; }

        public DateTime? ExpireDate { get; set; }

        public bool IsSent { get; set; }

        public DateTime? SentDate { get; set; }

        public bool IsUsed { get; set; }

        public DateTime? UsedDate { get; set; }
    }
}