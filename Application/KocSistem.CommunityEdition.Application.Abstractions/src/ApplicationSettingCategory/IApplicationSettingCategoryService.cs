﻿// <copyright file="IApplicationSettingCategoryService.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Application.Abstractions.ApplicationSettingCategory.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.OneFrame.Data.Relational;
using KocSistem.OneFrame.DesignObjects;
using KocSistem.OneFrame.DesignObjects.Services;
using System;
using System.Threading.Tasks;

namespace KocSistem.CommunityEdition.Application.Abstractions.ApplicationSettingCategory
{
    public interface IApplicationSettingCategoryService : IApplicationCrudServiceAsync<Domain.ApplicationSettingCategory, ApplicationSettingCategoryDto, Guid>, IApplicationService
    {
        Task<ServiceResponse<PagedResultDto<ApplicationSettingCategoryDto>>> GetApplicationSettingCategoryListAsync(PagedRequestDto pagedRequest);

        Task<ServiceResponse<ApplicationSettingCategoryDto>> CreateApplicationSettingCategoryAsync(ApplicationSettingCategoryDto applicationSettingCategoryDto);

        Task<ServiceResponse<ApplicationSettingCategoryDto>> UpdateApplicationSettingCategoryAsync(ApplicationSettingCategoryDto applicationSettingCategoryDto);

        Task<ServiceResponse> DeleteApplicationSettingCategoryAsync(Guid applicationSettingCategoryId);

        Task<ServiceResponse<ApplicationSettingCategoryDto>> GetApplicationSettingCategoryByIdAsync(Guid id);

        Task<ServiceResponse<PagedResultDto<ApplicationSettingCategoryDto>>> SearchAsync(ApplicationSettingCategorySearchDto applicationSettingCategoryGetRequest);
    }
}
