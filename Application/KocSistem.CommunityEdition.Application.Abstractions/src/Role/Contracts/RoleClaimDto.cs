﻿// <copyright file="RoleClaimDto.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KocSistem.CommunityEdition.Application.Abstractions.Role.Contracts
{
    public class RoleClaimDto
    {
        public string RoleName { get; set; }

        public string Name { get; set; }
    }
}